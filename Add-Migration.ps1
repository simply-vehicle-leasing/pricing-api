param([String]$Name)

. .\Include.ps1

if (!$Name) {
    throw "Must provide a migration name"
}

Ef "migrations add $Name"
