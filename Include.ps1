$ErrorActionPreference = "Stop"

Function IIf($If, $WhenTrue, $WhenFalse) {If ($If) {$WhenTrue} Else {$WhenFalse}}

<#
.SYNOPSIS
  This is a helper function that runs a scriptblock and checks the PS variable $lastexitcode
  to see if an error occcured. If an error is detected then an exception is thrown.
  This function allows you to run command-line programs without having to
  explicitly check the $lastexitcode variable.
.EXAMPLE
  exec { svn info $repository_trunk } "Error executing SVN. Please verify SVN command-line client is installed"
#>
function Exec
{
    [CmdletBinding()]
    param(
        [Parameter(Position=0,Mandatory=1)][scriptblock]$cmd,
        [Parameter(Position=1,Mandatory=0)][string]$errorMessage = ("Error executing command {0}" -f $cmd),
        [Parameter(Position=2,Mandatory=0)][scriptblock]$finally
    )
    & $cmd

    $return_code = $lastexitcode

    if ($finally) {
        & $finally
    }

    if ($return_code -ne 0) {
        throw ("Exec: " + $errorMessage)
    }
}

function Update-Aws-Credentials {
    $aws_credentials = "$ENV:UserProfile\.aws\credentials"

    If (!(Test-Path $aws_credentials)){
        throw "The AWS credentials file: $aws_credentials does not exist, install the AWS CLI: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html"
    }

    If (Test-Path ".env"){
        Remove-Item ".env"
    }

    foreach($line in Get-Content $aws_credentials) {
        if($line -match "^aws_access_key_id.+" -Or $line -match "^aws_secret_access_key.+"){
            $line -replace '\s','' | Out-File -Encoding utf8 ".env" -Append
        }
    }
}

function Publish
{
    [CmdletBinding()]
    param(
        [Parameter(Position=0,Mandatory=1)][string]$project
    )
    Exec {
        dotnet publish -c Release -o bin/publish "src/$project"
    } "Failed to publish $project"
}

function Test
{
    [CmdletBinding()]
    param(
        [Parameter(Position=0,Mandatory=1)][string]$project
    )
    Exec {
        dotnet test -c Release "src/$project"
    } "Tests failed in $project"
}

function Compose
{
    [CmdletBinding()]
    param(
        [Parameter(Position=0,Mandatory=0)][string]$id,
        [Parameter(Position=1,Mandatory=0)][string]$exitCodeFrom
    )

    $compose_cmd = "docker-compose -f docker-compose.yml" + (IIf $id " -f docker-compose.${id}.yml" "")
    $down_cmd = "down --remove-orphans --volumes"
    $up_cmd = "up --force-recreate --build --abort-on-container-exit" + (IIF $exitCodeFrom " --exit-code-from $exitCodeFrom" "")

    Exec {
        Invoke-Expression "$compose_cmd $down_cmd"
        Invoke-Expression "$compose_cmd $up_cmd"
    } "Failed to run compose: $compose_cmd"
}

function Ef
{
    [CmdletBinding()]
    param(
        [Parameter(Position=0,Mandatory=1)][string]$cmd
    )
    $env:ASPNETCORE_ENVIRONMENT = "Development"
    $dotnet_ef_cmd = "dotnet ef $cmd --project src/Pricing.Data --context Pricing.Data.Contexts.PricingContext --startup-project src/Pricing.Api"

    Write-Output "Running: $dotnet_ef_cmd"
    Exec {
        Invoke-Expression $dotnet_ef_cmd
    } "Failed to run dotnet ef $cmd"
}