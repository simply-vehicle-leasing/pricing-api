﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Pricing.Data.Contracts;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Rates;
using Pricing.Data.Models;
using Pricing.Services.Contracts;
using Pricing.Services.Crud;
using Pricing.Services.Models.Rate;

namespace Pricing.Services
{
    /// <summary>
    /// Service for managing rates.
    /// </summary>
    /// <seealso cref="EntityFrameworkCrudService{Rate, IRateRepository, RateDto, CreateRateRequest}" />
    /// <seealso cref="IRateService" />
    public class RateService : EntityFrameworkCrudService<Rate, IRateRepository, RateDto, CreateRateRequest>, IRateService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateService" /> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="httpContextAccessor">The HTTP context accessor.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        public RateService(IRateRepository repository,
                                    IMapper mapper,
                                    IHttpContextAccessor httpContextAccessor,
                                    ILoggerFactory loggerFactory) : base(repository, mapper, httpContextAccessor, loggerFactory)
        {
        }

        /// <summary>
        /// Gets all rates by the specified CAP code.
        /// </summary>
        /// <param name="capCode">The cap code.</param>
        /// <returns></returns>
        public async Task<ICollection<RateDto>> GetByCapCodeAsync(string capCode)
        {
            var models = await Repository.GetByCapCodeAsync(capCode, RequestAborted);
            return Mapper.Map<ICollection<RateDto>>(models);
        }

        /// <summary>
        /// Gets all rates by the specified vehicle.
        /// </summary>
        /// <param name="vehicleId">The vehicle identifier.</param>
        /// <returns></returns>
        public async Task<ICollection<RateDto>> GetByVehicleAsync(int vehicleId)
        {
            var models = await Repository.GetByVehicleAsync(vehicleId, RequestAborted);
            return Mapper.Map<ICollection<RateDto>>(models);
        }

        /// <summary>
        /// Deletes all rates with the specified provider and profiles.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="profiles">The profiles.</param>
        /// <returns></returns>
        public async Task DeleteByProfilesAsync(RateBookProvider provider, ICollection<ContractProfile> profiles)
        {
            var count = await Repository.DeleteByProfilesAsync(provider, profiles, RequestAborted);
            if (count > 0)
            {
                Logger.LogWarning($"Deleted {count} rates for {provider} with profiles {string.Join(", ", profiles)}");
            }
        }
    }
}
