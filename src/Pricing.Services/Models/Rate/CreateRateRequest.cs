﻿using Pricing.Data.Models;

namespace Pricing.Services.Models.Rate
{
    /// <summary>
    /// A request to create a rate.
    /// </summary>
    public class CreateRateRequest
    {
        /// <summary>
        /// Gets or sets the vehicle identifier.
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Gets or sets the CAP code.
        /// </summary>
        public string CapCode { get; set; }

        /// <summary>
        /// Gets or sets the contract term in months.
        /// </summary>
        public short Term { get; set; }

        /// <summary>
        /// Gets or sets the maximum annual mileage.
        /// </summary>
        public int Mileage { get; set; }

        /// <summary>
        /// Gets or sets the number of initial payments.
        /// </summary>
        public short InitialPayments { get; set; }

        /// <summary>
        /// Gets or sets the initial payment.
        /// </summary>
        public decimal InitialPayment { get; set; }

        /// <summary>
        /// Gets or sets the monthly payment.
        /// </summary>
        public decimal MonthlyPayment { get; set; }

        /// <summary>
        /// Gets or sets the total cost over the entire contract.
        /// </summary>
        public decimal TotalCost { get; set; }

        /// <summary>
        /// Gets or sets the cost excess mileage in pence/mile.
        /// </summary>
        public decimal ExcessMileageCost { get; set; }

        /// <summary>
        /// Gets or sets the value score.
        /// This is the ratio of the total contract cost vs the P11D price multiplied by 10000.
        /// Lower values are better value.
        /// This only makes sense to be compared with rates in the same term and mileage profile.
        /// </summary>
        public int? ValueScore { get; set; }

        /// <summary>
        /// Gets or sets the provider.
        /// </summary>
        public RateBookProvider Provider { get; set; }

        /// <summary>
        /// Gets the contract subject.
        /// </summary>
        public ContractSubject ContractSubject { get; set; }

        /// <summary>
        /// Gets or sets the contract features.
        /// </summary>
        public ContractFeatures ContractFeatures { get; set; }
    }
}
