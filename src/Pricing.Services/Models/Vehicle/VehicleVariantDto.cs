﻿namespace Pricing.Services.Models.Vehicle
{
    /// <summary>
    /// A vehicle variant.
    /// </summary>
    public class VehicleVariantDto
    {
        /// <summary>
        /// Gets or sets the cap code.
        /// </summary>
        public string CapCode { get; set; }

        /// <summary>
        /// Gets or sets the variant.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the P11D price.
        /// </summary>
        public decimal P11D { get; set; }
    }
}