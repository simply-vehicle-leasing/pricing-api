﻿namespace Pricing.Services.Models.Vehicle
{
    /// <summary>
    /// A request to create a vehicle.
    /// </summary>
    public class CreateVehicleVariantRequest
    {
        /// <summary>
        /// Gets or sets the CAP code.
        /// </summary>
        public string CapCode { get; set; }

        /// <summary>
        /// Gets or sets the hash.
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer of the vehicle.
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the model of the vehicle.
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Gets or sets the variant of the vehicle.
        /// </summary>
        public string Variant { get; set; }

        /// <summary>
        /// Gets the type of the body.
        /// </summary>
        public string BodyType { get; set; }

        /// <summary>
        /// Gets or sets the P11D price.
        /// </summary>
        public decimal? P11D { get; set; }
    }
}
