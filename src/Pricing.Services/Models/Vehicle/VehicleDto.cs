﻿using System.Collections.Generic;

namespace Pricing.Services.Models.Vehicle
{
    /// <summary>
    /// A vehicle.
    /// </summary>
    public class VehicleDto : VehicleSummaryDto
    {
        /// <summary>
        /// Gets or sets the variant of the vehicle.
        /// </summary>
        public ICollection<VehicleVariantDto> Variants { get; set; }
    }
}
