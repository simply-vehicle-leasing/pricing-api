﻿namespace Pricing.Services.Models.Vehicle
{
    /// <summary>
    /// A vehicle.
    /// </summary>
    public class VehicleSummaryDto
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer of the vehicle.
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the model of the vehicle.
        /// </summary>
        public string Model { get; set; }
    }
}
