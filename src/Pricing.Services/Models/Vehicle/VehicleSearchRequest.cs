﻿namespace Pricing.Services.Models.Vehicle
{
    /// <summary>
    /// A request to get a paginated collection of <see cref="VehicleSummaryDto"/>.
    /// </summary>
    /// <seealso cref="SearchRequest" />
    public class VehicleSearchRequest : SearchRequest
    {
        /// <summary>
        /// Gets or sets an optional vehicle manufacturer filter.
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets an optional vehicle model filter.
        /// </summary>
        public string Model { get; set; }
    }
}
