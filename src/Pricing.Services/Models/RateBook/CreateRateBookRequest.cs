﻿using System;
using System.Collections.Generic;
using Pricing.Data.Models;
using Pricing.Services.Models.Rate;

namespace Pricing.Services.Models.RateBook
{
    public class CreateRateBookRequest
    {
        /// <summary>
        /// Gets or sets a hash of the original pricing book file.
        /// </summary>
        public int FileHash { get; set; }

        /// <summary>
        /// Gets or sets the provider.
        /// </summary>
        public RateBookProvider Provider { get; set; }

        /// <summary>
        /// Gets or sets the filename of the pricing book.
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Gets or sets the size of the file in bytes.
        /// </summary>
        public int FileSize { get; set; }

        /// <summary>
        /// Gets or sets the date added.
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets the total number of parsed rates.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Gets or sets the invalid rates.
        /// </summary>
        public ICollection<InvalidRateBookRowDto> InvalidRows { get; set; }
    }
}
