﻿namespace Pricing.Services.Models.RateBook
{
    /// <summary>
    /// An invalid rate parsed from a pricing book.
    /// </summary>
    public class InvalidRateBookRowDto
    {
        /// <summary>
        /// Gets or sets the row number.
        /// </summary>
        public int RowNumber { get; set; }

        /// <summary>
        /// Gets or sets the CAP code.
        /// </summary>
        public string CapCode { get; set; }

        /// <summary>
        /// Gets or sets the validation errors.
        /// </summary>
        public string ValidationErrors { get; set; }
    }
}