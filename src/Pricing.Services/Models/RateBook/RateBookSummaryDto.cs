﻿using System;
using Pricing.Data.Models;

namespace Pricing.Services.Models.RateBook
{
    /// <summary>
    /// A summary of a pricing book.
    /// </summary>
    public class RateBookSummaryDto
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the provider.
        /// </summary>
        public RateBookProvider Provider { get; set; }

        /// <summary>
        /// Gets or sets the filename of the pricing book.
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Gets or sets the size of the file in bytes.
        /// </summary>
        public int FileSize { get; set; }

        /// <summary>
        /// Gets or sets the date added.
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets the rate count.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Gets or sets the invalid rate count.
        /// </summary>
        public int InvalidRowCount { get; set; }
    }
}
