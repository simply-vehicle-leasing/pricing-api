﻿using Microsoft.AspNetCore.Http;
using Pricing.Data.Models;

namespace Pricing.Services.Models.RateBook
{
    /// <summary>
    /// A request to parse a pricing book.
    /// </summary>
    public class EnqueueParseRateBookRequest
    {
        /// <summary>
        /// Gets or sets the provider.
        /// </summary>
        public RateBookProvider Provider { get; set; }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        public IFormFile File { get; set; }
    }
}
