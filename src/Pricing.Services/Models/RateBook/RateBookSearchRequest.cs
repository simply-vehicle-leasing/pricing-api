﻿using System;
using Pricing.Data.Models;

namespace Pricing.Services.Models.RateBook
{
    /// <summary>
    /// A request to get a paginated collection of <see cref="RateBookSummaryDto"/>.
    /// </summary>
    /// <seealso cref="SearchRequest" />
    public class RateBookSearchRequest : SearchRequest
    {
        /// <summary>
        /// Gets or sets an optional rate book provider filter.
        /// </summary>
        public RateBookProvider? Provider { get; set; }

        /// <summary>
        /// Gets or sets an optional filename search query.
        /// <c>null</c> means match all files.
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Gets or sets an optional minimum date added filter.
        /// </summary>
        public DateTime? MinimumDateAdded { get; set; }

        /// <summary>
        /// Gets or sets an optional maximum date added filter.
        /// </summary>
        public DateTime? MaximumDateAdded { get; set; }
    }
}
