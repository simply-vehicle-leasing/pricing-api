﻿using Pricing.Data.Models;

namespace Pricing.Services.Models.RateBook
{
    public class ParseRateBookRequest
    {
        /// <summary>
        /// Gets or sets the provider.
        /// </summary>
        public RateBookProvider Provider { get; set; }

        /// <summary>
        /// Gets or sets the file content.
        /// </summary>
        public byte[] FileContent { get; set; }

        /// <summary>
        /// Gets or sets the filename.
        /// </summary>
        public string Filename { get; set; }
    }
}