﻿using System.Collections.Generic;

namespace Pricing.Services.Models.RateBook
{
    /// <summary>
    /// A pricing book.
    /// </summary>
    public class RateBookDto : RateBookSummaryDto
    {
        /// <summary>
        /// Gets or sets the invalid rates.
        /// </summary>
        public ICollection<InvalidRateBookRowDto> InvalidRows { get; set; }
    }
}
