﻿using System;
using Pricing.Data.Entities.Background;

namespace Pricing.Services.Models.Background
{
    public class BackgroundQueueWorkItemDto
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the date time that the work item was queued.
        /// </summary>
        public DateTime Queued { get; set; }

        /// <summary>
        /// Gets or sets the date time that the work item was executed.
        /// </summary>
        public DateTime? Executed { get; set; }

        /// <summary>
        /// Gets or sets the date time that the work item was completed.
        /// </summary>
        public DateTime? Completed { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public BackgroundQueueWorkItemStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the queued time.
        /// </summary>
        public TimeSpan QueuedTime { get; set; }

        /// <summary>
        /// Gets or sets the execution time.
        /// </summary>
        public TimeSpan ExecutionTime { get; set; }
    }
}
