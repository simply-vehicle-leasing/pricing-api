﻿using System.Collections.Generic;

namespace Pricing.Services.Models
{
    /// <summary>
    /// A paginated collection of models.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    public class PaginatedCollectionDto<TModel>
    {
        /// <summary>
        /// Gets or sets the models found on the current page.
        /// </summary>
        public ICollection<TModel> Models { get; set; }

        /// <summary>
        /// Gets or sets the current (1-based) page number.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Gets or sets the total count of all records in the collection.
        /// </summary>
        public int TotalCount { get; set; }
    }
}
