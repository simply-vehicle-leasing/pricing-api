﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Pricing.Data.Contracts;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities;
using Pricing.Services.Models;

namespace Pricing.Services.Crud
{
    /// <summary>
    /// A searchable CRUD service that consumes an EF repository.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TRepository">The type of the repository.</typeparam>
    /// <typeparam name="TDto">The type of the dto.</typeparam>
    /// <typeparam name="TCreateRequest">The type of the create request.</typeparam>
    /// <typeparam name="TGetPaginatedCollectionRequest">The type of the get paginated collection request.</typeparam>
    /// <typeparam name="TSearchRequest">The type of the search request.</typeparam>
    /// <seealso cref="Pricing.Services.Crud.EntityFrameworkCrudService{TEntity, TRepository, TDto, TCreateRequest}" />
    public abstract class SearchableEntityFrameworkCrudService<TEntity, TRepository, TDto, TCreateRequest, TGetPaginatedCollectionRequest, TSearchRequest>
        : EntityFrameworkCrudService<TEntity, TRepository, TDto, TCreateRequest>
        where TEntity : class, IEntity
        where TRepository : ISearchableEntityFrameworkRepository<TEntity, TGetPaginatedCollectionRequest>
        where TSearchRequest : SearchRequest, new()
        where TGetPaginatedCollectionRequest : GetPaginatedCollectionRequest
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchableEntityFrameworkCrudService{TEntity, TRepository, TDto, TCreateRequest, TGetPaginatedCollectionRequest, TSearchRequest}" /> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="httpContextAccessor">The HTTP context accessor.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        protected SearchableEntityFrameworkCrudService(TRepository repository, IMapper mapper, IHttpContextAccessor httpContextAccessor, ILoggerFactory loggerFactory)
            : base(repository, mapper, httpContextAccessor, loggerFactory)
        {
        }

        /// <summary>
        /// Searches for entities according to the specified <typeparamref name="TSearchRequest"/>.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<PaginatedCollectionDto<TDto>> SearchAsync(TSearchRequest request)
        {
            var getCollectionRequest = Mapper.Map<TGetPaginatedCollectionRequest>(request);
            var result = await Repository.SearchAsync(getCollectionRequest, RequestAborted);
            return Mapper.Map<PaginatedCollectionDto<TDto>>(result);
        }
    }
}