﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Pricing.Data.Contracts;
using Pricing.Data.Entities;
using Pricing.Services.Contracts.Exceptions;

namespace Pricing.Services.Crud
{
    /// <summary>
    /// A CRUD service that consumes an EF repository.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TRepository">The type of the repository.</typeparam>
    /// <typeparam name="TDto">The type of the dto.</typeparam>
    /// <typeparam name="TCreateRequest">The type of the create request.</typeparam>
    public abstract class EntityFrameworkCrudService<TEntity, TRepository, TDto, TCreateRequest>
        where TEntity : class, IEntity
        where TRepository : IEntityFrameworkRepository<TEntity>
    {
        /// <summary>
        /// The repository
        /// </summary>
        protected readonly TRepository Repository;

        /// <summary>
        /// The mapper
        /// </summary>
        protected readonly IMapper Mapper;

        /// <summary>
        /// The logger
        /// </summary>
        protected readonly ILogger Logger;

        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityFrameworkCrudService{TEntity,TRepository,TDto,TCreateRequest}" /> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="httpContextAccessor">The HTTP context accessor.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        protected EntityFrameworkCrudService(TRepository repository, IMapper mapper, IHttpContextAccessor httpContextAccessor, ILoggerFactory loggerFactory)
        {
            Repository = repository;
            Mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
            Logger = loggerFactory.CreateLogger(GetType());
        }

        /// <summary>
        /// Creates a new <typeparamref name="TDto"/> according to the specified <typeparamref name="TCreateRequest"/>.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public virtual async Task<TDto> CreateAsync(TCreateRequest request)
        {
            var item = Mapper.Map<TEntity>(request);

            try
            {
                await Repository.AddAsync(item, RequestAborted);
            }
            catch (DbUpdateException e)
            {
                switch (e.InnerException)
                {
                    case MySqlException me:
                        HandleMySqlException(item, me);
                        break;
                }
                throw;
            }

            return Mapper.Map<TDto>(item);
        }

        /// <summary>
        /// Creates new <typeparamref name="TDto"/>'s according to the specified <typeparamref name="TCreateRequest"/>'s.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <returns></returns>
        public virtual async Task<ICollection<TDto>> CreateManyAsync(ICollection<TCreateRequest> requests)
        {
            if (!requests.Any())
            {
                return Array.Empty<TDto>();
            }

            var items = Mapper.Map<ICollection<TEntity>>(requests);

            try
            {
                await Repository.AddRangeAsync(items, RequestAborted);
            }
            catch (DbUpdateException e)
            {
                switch (e.InnerException)
                {
                    case MySqlException me:
                        HandleMySqlException(items.First(), me);
                        break;
                }
                throw;
            }

            return Mapper.Map<ICollection<TDto>>(items);
        }

        /// <summary>
        /// Gets all <typeparamref name="TDto"/>.
        /// </summary>
        /// <returns></returns>
        public virtual async Task<ICollection<TDto>> GetAllAsync()
        {
            var items = await Repository.GetAllAsync(RequestAborted);
            return Mapper.Map<ICollection<TDto>>(items);
        }

        /// <summary>
        /// Gets the <typeparamref name="TDto"/> with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public virtual async Task<TDto> GetAsync(int id)
        {
            var item = await Repository.GetAsync(id, RequestAborted);
            if (item == null)
            {
                throw EntityNotFoundException.Create<TEntity>(x => x.Id, id);
            }

            return Mapper.Map<TDto>(item);
        }

        /// <summary>
        /// Deletes the <typeparamref name="TDto"/> with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public virtual async Task DeleteAsync(int id)
        {
            var entity = await Repository.GetAsync(id, RequestAborted);
            if (entity == null)
            {
                throw EntityNotFoundException.Create<TEntity>(x => x.Id, id);
            }

            await Repository.DeleteAsync(entity, RequestAborted);
        }

        /// <summary>
        /// Handles a MySQL exception thrown on model creation or update.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="exception">The exception.</param>
        protected virtual void HandleMySqlException(TEntity entity, MySqlException exception)
        {
            Match match = null;
            switch ((MySqlErrorCode)exception.Number)
            {
                case MySqlErrorCode.DuplicateKeyEntry:
                    throw BadRequestException.Duplicate<TEntity>(exception);

                case MySqlErrorCode.NoReferencedRow2:
                case MySqlErrorCode.NoReferencedRow:
                    match = Regex.Match(exception.Message, @"FOREIGN KEY \(`(.+)`\)");
                    if (match.Success)
                    {
                        throw BadRequestException.Invalid(match.Groups[1].Value, exception);
                    }
                    throw BadRequestException.Invalid<TEntity>(exception);

                default:
                    Logger.LogInformation(exception, "Unexpected MySQL error");

                    match = Regex.Match(exception.Message, @"Column `(.+)`");
                    if (match.Success)
                    {
                        throw BadRequestException.Invalid(match.Groups[1].Value, exception);
                    }
                    throw BadRequestException.Invalid<TEntity>(exception);

            }
        }

        /// <summary>
        /// Gets the request aborted cancellation token.
        /// </summary>
        protected CancellationToken RequestAborted => _httpContextAccessor.HttpContext?.RequestAborted ?? CancellationToken.None;
    }
}
