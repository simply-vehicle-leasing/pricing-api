﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Pricing.Services.Models;
using Pricing.Services.Models.Vehicle;

namespace Pricing.Services.Contracts
{
    /// <summary>
    /// Service for managing vehicles.
    /// </summary>
    public interface IVehicleService
    {
        /// <summary>
        /// Searches for entities as <see cref="VehicleSummaryDto"/> according to the specified <see cref="VehicleSearchRequest"/>.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<PaginatedCollectionDto<VehicleSummaryDto>> SearchAsync(VehicleSearchRequest request);
        
        /// <summary>
        /// Gets all <see cref="VehicleDto"/>.
        /// </summary>
        /// <returns></returns>
        Task<ICollection<VehicleSummaryDto>> GetAllAsync();

        /// <summary>
        /// Gets the <see cref="VehicleDto"/> with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<VehicleSummaryDto> GetAsync(int id);

        /// <summary>
        /// Deletes the <see cref="VehicleDto"/> with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task DeleteAsync(int id);

        /// <summary>
        /// Inserts or updates the specified vehicles.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <returns></returns>
        Task InsertOrUpdateManyAsync(ICollection<CreateVehicleVariantRequest> requests);

        /// <summary>
        /// Gets all vehicle models.
        /// </summary>
        /// <returns></returns>
        Task<ICollection<string>> GetAllModelsAsync();

        /// <summary>
        /// Gets all vehicle manufacturers.
        /// </summary>
        /// <returns></returns>
        Task<ICollection<string>> GetAllManufacturersAsync();

        /// <summary>
        /// Gets the <see cref="VehicleDto"/> with the specified identifier, including all variants.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<VehicleDto> GetDetailAsync(int id);
    }
}