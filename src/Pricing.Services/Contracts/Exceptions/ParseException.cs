﻿using System;

namespace Pricing.Services.Contracts.Exceptions
{
    public class ParseException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParseException" /> class.
        /// </summary>
        /// <param name="filename">Name of the file.</param>
        /// <param name="inner">The inner exception.</param>
        /// <inheritdoc />
        public ParseException(string filename, Exception inner = null) : base(GetMessage(filename), inner)
        {
        }

        private static string GetMessage(string fileName) => $"Failed to parse {fileName}";
    }
}
