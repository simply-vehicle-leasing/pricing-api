﻿using System;
using System.Linq;
using System.Linq.Expressions;
using FluentValidation.Results;
using Humanizer;
using Pricing.Services.Extensions;

namespace Pricing.Services.Contracts.Exceptions
{
    /// <summary>
    /// An exception indicating a bad request with validation failures for messaging.
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class BadRequestException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BadRequestException" /> class.
        /// </summary>
        /// <param name="inner">The inner.</param>
        /// <param name="validationFailures">The validation failures.</param>
        /// <exception cref="ArgumentException">The validation result is valid - validationFailures</exception>
        public BadRequestException(Exception inner, params ValidationFailure[] validationFailures)
            : base(string.Join(", ", validationFailures.Select(f => $"{f.PropertyName}: {f.ErrorMessage}")), inner)
        {
            if (!validationFailures.Any())
            {
                throw new ArgumentException("The validation result is valid", nameof(validationFailures));
            }

            Validation = new ValidationResult(validationFailures);
        }

        /// <summary>
        /// Gets the validation.
        /// </summary>
        public ValidationResult Validation { get; }

        /// <summary>
        /// Creates a bad request exception informing of a duplicate <see cref="TEntity" /> of the specified value.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <param name="inner">The inner.</param>
        /// <returns></returns>
        public static BadRequestException Duplicate<TEntity>(Expression<Func<TEntity, object>> property, object value, Exception inner = null) =>
            Duplicate<TEntity>(property.GetMemberName(), value, inner);

        /// <summary>
        /// Creates a bad request exception informing of a duplicate <see cref="TEntity" /> of the specified value.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <param name="inner">The inner.</param>
        /// <returns></returns>
        public static BadRequestException Duplicate<TEntity>(string property, object value, Exception inner = null) =>
            new BadRequestException(inner, GetDuplicateFailure<TEntity>(property, value));

        /// <summary>
        /// Creates a bad request exception informing of a duplicate <see cref="TEntity" /> of the specified value.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="inner">The inner.</param>
        /// <param name="properties">The properties.</param>
        /// <returns></returns>
        public static BadRequestException Duplicate<TEntity>(Exception inner, params (Expression<Func<TEntity, object>> property, object value)[] properties)
        {
            var errors = properties.Select(x => GetDuplicateFailure<TEntity>(x.property.GetMemberName(), x.value, true)).ToArray();
            return new BadRequestException(inner, errors);
        }

        /// <summary>
        /// Creates a bad request exception informing of a duplicate <see cref="TEntity" /> of the specified value.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="inner">The inner.</param>
        /// <returns></returns>
        public static BadRequestException Duplicate<TEntity>(Exception inner = null) =>
            new BadRequestException(inner, new ValidationFailure(string.Empty, $"Duplicate {GetTypeName<TEntity>()}"));

        /// <summary>
        /// Creates a bad request exception informing of missing data.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="property">The property.</param>
        /// <param name="inner">The inner.</param>
        /// <returns></returns>
        public static BadRequestException MissingData<TEntity>(Expression<Func<TEntity, object>> property, Exception inner = null)
        {
            var name = property.GetMemberName();
            return new BadRequestException(inner, new ValidationFailure(name, $"Missing {GetTypeName<TEntity>()} {name.Humanize()}"));
        }

        /// <summary>
        /// Creates a bad request exception informing of bad data.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static BadRequestException BadData<TData>(string name, TData value) =>
            new BadRequestException(null, new ValidationFailure(name, $"Bad {name}: {value}"));

        /// <summary>
        /// Creates a bad request exception informing that something was invalid.
        /// </summary>
        /// <param name="name">The name of the invalid object.</param>
        /// <param name="inner">The inner.</param>
        /// <returns></returns>
        public static BadRequestException Invalid(string name, Exception inner = null) =>
            new BadRequestException(inner, new ValidationFailure(string.Empty, $"Invalid {name.Humanize()}"));

        /// <summary>
        /// Creates a bad request exception informing that something was invalid.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="inner">The inner.</param>
        /// <returns></returns>
        public static BadRequestException Invalid<TEntity>(Exception inner = null) =>
            new BadRequestException(inner, new ValidationFailure(string.Empty, $"Invalid {GetTypeName<TEntity>()}"));

        private static ValidationFailure GetDuplicateFailure<TEntity>(string property, object value, bool isComposite = false) =>
            new ValidationFailure(property, $"A {GetTypeName<TEntity>()} already exists with {(isComposite ? "composite key" : "key")}: {property.Humanize()} and value: {value}");

        private static string GetTypeName<TEntity>() => typeof(TEntity).Name.Humanize();
    }
}
