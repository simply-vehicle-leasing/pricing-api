﻿using System.Threading.Tasks;
using Pricing.Data.Entities.Background;
using Pricing.Services.Models.RateBook;

namespace Pricing.Services.Contracts
{
    /// <summary>
    /// Service for parsing rate books.
    /// </summary>
    public interface IRateBookParsingBackgroundQueueService
    {
        /// <summary>
        /// Parses a rate book according to the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        void EnqueueRateBookParsingRequest(ParseRateBookRequest request, BackgroundQueueWorkItem item);
    }
}