﻿using System.Collections.Generic;
using Pricing.Services.Models.Rate;
using Pricing.Services.Models.RateBook;
using Pricing.Services.Models.Vehicle;

namespace Pricing.Services.Contracts
{
    public class ProviderRateBookParserResult
    {
        /// <summary>
        /// Gets or sets the pricing book that was parsed.
        /// </summary>
        public CreateRateBookRequest RateBook { get; set; }

        /// <summary>
        /// Gets or sets the vehicles that were parsed.
        /// </summary>
        public ICollection<CreateVehicleVariantRequest> Vehicles { get; set; }

        /// <summary>
        /// Gets or sets the rates that were parsed.
        /// </summary>
        public ICollection<CreateRateRequest> Rates { get; set; }
    }
}
