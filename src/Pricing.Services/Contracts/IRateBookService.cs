﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Pricing.Services.Models;
using Pricing.Services.Models.RateBook;

namespace Pricing.Services.Contracts
{
    /// <summary>
    /// Service for managing rate books.
    /// </summary>
    public interface IRateBookService
    {
        /// <summary>
        /// Searches for entities as <see cref="RateBookSummaryDto"/> according to the specified <see cref="RateBookSearchRequest"/>.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<PaginatedCollectionDto<RateBookSummaryDto>> SearchAsync(RateBookSearchRequest request);

        /// <summary>
        /// Creates a new <see cref="RateBookDto"/> according to the specified <see cref="CreateRateBookRequest"/>.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<RateBookSummaryDto> CreateAsync(CreateRateBookRequest request);

        /// <summary>
        /// Gets all <see cref="RateBookDto"/>.
        /// </summary>
        /// <returns></returns>
        Task<ICollection<RateBookSummaryDto>> GetAllAsync();

        /// <summary>
        /// Gets the <see cref="RateBookDto"/> with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<RateBookSummaryDto> GetAsync(int id);

        /// <summary>
        /// Deletes the <see cref="RateBookDto"/> with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task DeleteAsync(int id);

        /// <summary>
        /// Gets the rate book detail for the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<RateBookDto> GetDetailAsync(int id);
    }
}