﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Pricing.Services.Models.Background;
using Pricing.Services.Models.RateBook;

namespace Pricing.Services.Contracts
{
    /// <summary>
    /// Service for parsing rate books.
    /// </summary>
    public interface IRateBookParsingService
    {
        /// <summary>
        /// Parses a rate book according to the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<BackgroundQueueWorkItemDto> EnqueueRateBookParsingRequestAsync(EnqueueParseRateBookRequest request);

        /// <summary>
        /// Gets all queued rate book parsing requests.
        /// </summary>
        /// <param name="limit">The limit.</param>
        /// <returns></returns>
        Task<ICollection<BackgroundQueueWorkItemDto>> GetAllQueuedRateBookParsingRequestsAsync(int limit);

        /// <summary>
        /// Executes the specified work item.
        /// </summary>
        /// <param name="request">The work item.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task ParseRateBookAsync(ParseRateBookRequest request, CancellationToken cancellationToken);

        /// <summary>
        /// Gets the queued rate book parsing request with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<BackgroundQueueWorkItemDto> GetQueuedRateBookParsingRequestAsync(int id);
    }
}