﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Vehicles;
using Pricing.Data.Models;

namespace Pricing.Services.Contracts
{
    /// <summary>
    /// A parser for CAP keyed <see cref="Vehicle"/>.
    /// </summary>
    public interface IProviderRateBookParser
    {
        /// <summary>
        /// Determines whether the specified filename is valid for parsing with this parser.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>
        ///   <c>true</c> if the specified filename is valid for parsing with this parser; otherwise, <c>false</c>.
        /// </returns>
        bool IsValidFilename(string filename);

        /// <summary>
        /// Gets the provider.
        /// </summary>
        RateBookProvider Provider { get; }

        /// <summary>
        /// Parses all records in the specified content stream with associated filename.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<ProviderRateBookParserResult> ParseRateBookAsync(string filename, Stream stream, CancellationToken cancellationToken);
    }
}
