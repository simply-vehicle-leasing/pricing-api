﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Pricing.Data.Contracts;
using Pricing.Data.Models;
using Pricing.Services.Models.Rate;

namespace Pricing.Services.Contracts
{
    /// <summary>
    /// Service for managing rates.
    /// </summary>
    public interface IRateService
    {
        /// <summary>
        /// Creates a new <see cref="RateDto"/> according to the specified <see cref="CreateRateRequest"/>.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        Task<RateDto> CreateAsync(CreateRateRequest request);

        /// <summary>
        /// Creates new <see cref="RateDto"/>'s according to the specified <see cref="CreateRateRequest"/>'s.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <returns></returns>
        Task<ICollection<RateDto>> CreateManyAsync(ICollection<CreateRateRequest> requests);

        /// <summary>
        /// Gets all <see cref="RateDto"/>.
        /// </summary>
        /// <returns></returns>
        Task<ICollection<RateDto>> GetAllAsync();

        /// <summary>
        /// Gets the <see cref="RateDto"/> with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<RateDto> GetAsync(int id);

        /// <summary>
        /// Deletes the <see cref="RateDto"/> with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task DeleteAsync(int id);

        /// <summary>
        /// Gets all rates by the specified CAP code.
        /// </summary>
        /// <param name="capCode">The cap code.</param>
        /// <returns></returns>
        Task<ICollection<RateDto>> GetByCapCodeAsync(string capCode);

        /// <summary>
        /// Gets all rates by the specified vehicle.
        /// </summary>
        /// <param name="vehicleId">The vehicle identifier.</param>
        /// <returns></returns>
        Task<ICollection<RateDto>> GetByVehicleAsync(int vehicleId);

        /// <summary>
        /// Deletes all rates with the specified provider and profiles.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="profiles">The profiles.</param>
        /// <returns></returns>
        Task DeleteByProfilesAsync(RateBookProvider provider, ICollection<ContractProfile> profiles);
    }
}