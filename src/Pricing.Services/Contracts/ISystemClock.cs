﻿using System;

namespace Pricing.Services.Contracts
{
    public interface ISystemClock
    {
        /// <summary>
        /// Gets the current UTC date time.
        /// </summary>
        DateTimeOffset UtcNow { get; }
    }
}