﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Pricing.Services.Tabular
{
    internal static class ExcelFilenameValidator
    {
        /// <summary>
        /// Determines whether the specified filename is an Excel file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>
        ///   <c>true</c> if the specified filename is an Excel file; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValid(string filename)
        {
            switch (Path.GetExtension(filename)?.ToLower())
            {
                case ".xls":
                case ".xlsx":
                case ".xlsm":
                    return true;
                default:
                    return false;
            }
        }
    }
}
