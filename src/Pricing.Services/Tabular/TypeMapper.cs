﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using AgileObjects.ReadableExpressions.Extensions;

namespace Pricing.Services.Tabular
{
    internal class TypeMapperSet
    {
        private readonly ICollection<ITypeMapper> _mappers;

        public TypeMapperSet()
        {
            _mappers = new List<ITypeMapper>();
        }

        public TypeMapperSet WithExactDates(string dateFormat) => Add(new ExactDateTimeOffsetMapper(dateFormat));

        public TypeMapperSet WithSafeDecimals(int decimalPrecision) => Add(new SafeDecimalTypeMapper(decimalPrecision));

        public TypeMapperSet Add(ITypeMapper mapper)
        {
            _mappers.Add(mapper);
            return this;
        }

        public TypeMapperSet Add<TMapper>()
            where TMapper : class, ITypeMapper, new() => Add(new TMapper());

        public TTarget SafeConvert<TTarget>(object value)
        {
            var target = typeof(TTarget);
            var type = value?.GetType();
            if (type == target)
            {
                return (TTarget) value;
            }

            // As a special case, convert an empty or whitespace filled string to null where possible
            // (except if the target type is also string, which would be handled above)
            if (value == null || type == typeof(string) && string.IsNullOrWhiteSpace((string)value))
            {
                if (target.CanBeNull())
                {
                    return default;
                }

                throw new ArgumentException($"Failed to convert from \"{value ?? "<null>"}\" ({type}) to {target}. Type cannot be null.");
            }

            try
            {
                var unwrappedTarget = Nullable.GetUnderlyingType(target) ?? target;
                var mapper = _mappers.FirstOrDefault(m => m.CanMap(type, unwrappedTarget));
                if (mapper != null)
                {
                    return (TTarget) mapper.Map(value);
                }

                if (unwrappedTarget.IsEnum)
                {
                    return (TTarget) Enum.ToObject(unwrappedTarget, Convert.ChangeType(value, typeof(int)));
                }

                return (TTarget) Convert.ChangeType(value, unwrappedTarget);
            }
            catch (Exception e)
            {
                throw new ArgumentException($"Failed to convert from \"{value}\" ({type}) to {target}", e);
            }
        }
    }

    public interface ITypeMapper
    {
        bool CanMap(Type source, Type target);

        object Map(object value);
    }

    public class SafeDecimalTypeMapper : ITypeMapper
    {
        private readonly int _decimalPrecision;

        public SafeDecimalTypeMapper(int decimalPrecision)
        {
            _decimalPrecision = decimalPrecision;
        }

        public bool CanMap(Type source, Type target) => source == typeof(double) && target == typeof(decimal);

        public object Map(object value) => Math.Round((decimal) (double) value, _decimalPrecision);
    }

    public class ExactDateTimeOffsetMapper : ITypeMapper
    {
        private readonly string _dateFormat;

        public ExactDateTimeOffsetMapper(string dateFormat)
        {
            _dateFormat = dateFormat;
        }

        public bool CanMap(Type source, Type target) => source == typeof(string) && target == typeof(DateTimeOffset);

        public object Map(object value) => DateTimeOffset.ParseExact(value.ToString(), _dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal);
    }

    public class DateTimeOffsetMapper : ITypeMapper
    {
        public bool CanMap(Type source, Type target) => source == typeof(DateTime) && target == typeof(DateTimeOffset);

        public object Map(object value) => new DateTimeOffset(DateTime.SpecifyKind((DateTime) value, DateTimeKind.Utc), TimeSpan.Zero);
    }

    public class EnumTypeMapper<TEnum> : ITypeMapper
    {
        private readonly IDictionary<string, TEnum> _mappings;

        public EnumTypeMapper()
        {
            _mappings = new Dictionary<string, TEnum>();

            // Add from attributes.
            foreach (var value in Enum.GetValues(typeof(TEnum)).Cast<TEnum>())
            {
                var meta = typeof(TEnum).GetMember(value.ToString()).Single().GetCustomAttribute<EnumMapAttribute>();
                if (meta != null)
                {
                    _mappings.Add(CleanString(meta.Name), value);
                }
            }
        }

        public EnumTypeMapper<TEnum> Map(string name, TEnum value)
        {
            _mappings.Add(CleanString(name), value);
            return this;
        }

        public EnumTypeMapper<TEnum> MapAll(IDictionary<string, TEnum> mappings)
        {
            foreach (var mapping in mappings)
            {
                _mappings.Add(CleanString(mapping.Key), mapping.Value);
            }
            return this;
        }

        public bool CanMap(Type source, Type target) => source == typeof(string) && target == typeof(TEnum);

        public object Map(object value)
        {
            var name = CleanString((string)value);
            var result = _mappings.FirstOrDefault(kvp => kvp.Key.Equals(name, StringComparison.OrdinalIgnoreCase)).Value;
            if (result.Equals(default(TEnum)))
            {
                throw new ArgumentOutOfRangeException(nameof(value), value, $"Not a member of mappings for {typeof(TEnum)}");
            }
            return result;
        }

        private static string CleanString(string s) => Regex.Replace(s, @"[^a-zA-Z0-9_.]+", "");
    }

    public class EnumMapAttribute : Attribute
    {
        public EnumMapAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
