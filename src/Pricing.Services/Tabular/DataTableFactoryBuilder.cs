﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Pricing.Services.Extensions;

namespace Pricing.Services.Tabular
{
    /// <summary>
    /// A utility class for constructing a <see cref="DataTableFactory{TModel}"/> from a header row.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    internal class DataTableFactoryBuilder<TModel> where TModel : class, new()
    {
        private readonly ICollection<(int index, string name)> _header;
        private readonly IDictionary<int, (string name, PropertyInfo member)> _maps;
        private readonly TypeMapperSet _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataTableFactoryBuilder{TModel}" /> class.
        /// </summary>
        /// <param name="headerRow">The header row.</param>
        /// <param name="decimalPrecision">The decimal precision.</param>
        /// <param name="dateFormat">The date format.</param>
        public DataTableFactoryBuilder(DataRow headerRow, int decimalPrecision, string dateFormat)
        {
            _header = headerRow.ItemArray.Select((o, i) => (index: i, name: o.ToString()))
                               .Where(x => !string.IsNullOrWhiteSpace(x.name?.ToString()))
                               .ToList();
            if (!_header.Any())
            {
                throw new ArgumentException("Must provide header rows with some non-null or empty values");
            }
            _maps = new Dictionary<int, (string, PropertyInfo)>();
            _mapper = new TypeMapperSet().Add<DateTimeOffsetMapper>().WithExactDates(dateFormat).WithSafeDecimals(decimalPrecision);
        }

        /// <summary>
        /// Maps the specified column name to the specified property of <typeparamref name="TModel" />.
        /// </summary>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="column">The column.</param>
        /// <param name="propertyExpression">The property expression.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public DataTableFactoryBuilder<TModel> Map<TProperty>(ISubTableColumn column, Expression<Func<TModel, TProperty>> propertyExpression) =>
            Map(column, propertyExpression.GetPropertyInfo());

        /// <summary>
        /// Maps the specified column name to the specified property of <typeparamref name="TModel" />.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="member">The member.</param>
        /// <returns></returns>
        /// <remarks>
        /// Even if required is false, an ambiguous match or attempt to map a column that is already mapped will cause an exception
        /// </remarks>
        public DataTableFactoryBuilder<TModel> Map(ISubTableColumn column, PropertyInfo member)
        {
            if (member == null)
            {
                throw new ArgumentNullException(nameof(member));
            }

            var regex = column.GetRegex();
            var matches = _header.Where(x => regex.IsMatch(x.name)).ToList();
            if (!matches.Any())
            {
                return column.Required ? throw new ArgumentException($"There are no columns that match {column.Regex}") : this;
            }

            var firstMatch = matches.First();
            var index = firstMatch.index;
            if (column.Occurrence > 0)
            {
                if (matches.Count < column.Occurrence)
                {
                    return column.Required ? throw new ArgumentException($"There is no occurrence number {column.Occurrence} of column {column.Regex}; that column name appears only {matches.Count} times") : this;
                }

                index = matches[column.Occurrence - 1].index;
            }
            else if (matches.Count > 1)
            {
                var matchesString = string.Join(", ", matches.Select(m => $"\"{m.name}\""));
                throw new ArgumentException($"There are multiple columns that match {column.Regex}: {matchesString}");
            }

            if (_maps.ContainsKey(index))
            {
                throw new ArgumentException($"Column {index} that matches: {column.Regex} has already been mapped");
            }
            _maps.Add(index, (firstMatch.name, member));
            return this;
        }

        /// <summary>
        /// Adds a type mapper to the mapping set.
        /// </summary>
        /// <param name="mapper">The mapper.</param>
        /// <returns></returns>
        public DataTableFactoryBuilder<TModel> WithTypeMapper(ITypeMapper mapper)
        {
            _mapper.Add(mapper);
            return this;
        }

        public DataTableFactoryBuilder<TModel> WithTypeMapper<TMapper>() where TMapper : class, ITypeMapper, new()
        {
            _mapper.Add<TMapper>();
            return this;
        }

        /// <summary>
        /// Builds this factory using the maps provided with <see cref="Map{TProperty}"/>.
        /// </summary>
        /// <returns></returns>
        public DataTableFactory<TModel> Build()
        {
            var type = typeof(TModel);
            var row = Expression.Parameter(typeof(DataRow), "row");
            var mapper = Expression.Parameter(typeof(TypeMapperSet), "mapper");

            var isNullMethod = typeof(DataRow).GetMethod(nameof(DataRow.IsNull), new[] {typeof(int)})
                               ?? throw new ArgumentException($"Cannot find {nameof(DataRow)}.{nameof(DataRow.IsNull)}(int)");
            var safeConvertMethod = typeof(TypeMapperSet).GetMethod(nameof(TypeMapperSet.SafeConvert))
                                    ?? throw new ArgumentException($"Cannot find {nameof(TypeMapperSet)}.{nameof(TypeMapperSet.SafeConvert)}");
            var itemArrayPropertyInfo = typeof(DataRow).GetProperty(nameof(DataRow.ItemArray))
                                        ?? throw new ArgumentException($"Cannot find {nameof(DataRow)}.{nameof(DataRow.ItemArray)}");
            var itemArrayProperty = Expression.Property(row, itemArrayPropertyInfo);

            var bindings = _maps
                .Select(kvp =>
                {
                    var index = Expression.Constant(kvp.Key);
                    var isNull = Expression.Call(row, isNullMethod, index);
                    var value = Expression.Condition(isNull, Expression.Constant(null), Expression.ArrayAccess(itemArrayProperty, index));
                    var converted = Expression.Call(mapper, safeConvertMethod.MakeGenericMethod(kvp.Value.member.PropertyType), value);
                    return (MemberBinding)Expression.Bind(kvp.Value.member, converted);
                }).ToArray();

            
            var initExpression = Expression.MemberInit(Expression.New(type), bindings);
            var lambda = Expression.Lambda<Func<DataRow, TypeMapperSet, TModel>>(initExpression, row, mapper);
            var factoryFunction = lambda.Compile();
            var schema = _maps.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.name);

            return new DataTableFactory<TModel>(factoryFunction, _mapper, schema);
        }
    }
}
