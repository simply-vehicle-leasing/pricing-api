﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ExcelDataReader;

namespace Pricing.Services.Tabular
{
    /// <inheritdoc cref="DataSet" />
    /// <summary>
    /// Wrapper around <see cref="IExcelDataReader" /> so we're only exposing the <see cref="DataSet" /> based access and have run necessary setup.
    /// </summary>
    /// <seealso cref="T:System.IDisposable" />
    internal class ExcelDataSet : DataSet, IDisposable
    {
        static ExcelDataSet()
        {
            // This has to be called for the excel parsing library to work... something about old dos encodings in xls files.
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        }

        private readonly IExcelDataReader _reader;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelDataSet" /> class.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="sheetExpressions">Regular expressions identifying the sheets to read. If none provided then all sheets are read.</param>
        public ExcelDataSet(string filename, Stream stream, params string[] sheetExpressions)
        {
            var config = GetConfig(sheetExpressions);

            try
            {
                _reader = ExcelReaderFactory.CreateReader(stream);
                using (var set = _reader.AsDataSet(config))
                {
                    Merge(set);
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException("Invalid Excel workbook: " + filename, e);
            }

            if (Tables.Count == 0)
            {
                throw new ArgumentException("No worksheets in workbook: " + filename);
            }
        }

        private static ExcelDataSetConfiguration GetConfig(string[] sheetExpressions)
        {
            if (!sheetExpressions.Any())
            {
                return null;
            }

            var expressions = sheetExpressions.Select(r => new Regex(r, RegexOptions.Compiled | RegexOptions.IgnoreCase)).ToList();
            return new ExcelDataSetConfiguration
                   {
                       ConfigureDataTable = f => expressions.Any(r => r.IsMatch(f.Name))
                                                     ? new ExcelDataTableConfiguration {FilterRow = _ => true}
                                                     : new ExcelDataTableConfiguration {FilterRow = _ => false}
                   };
        }

        public new void Dispose()
        {
            base.Dispose();
            _reader?.Dispose();
        }
    }
}
