﻿using System;

namespace Pricing.Services.Tabular
{
    internal class SubTableColumnAttribute : Attribute, ISubTableColumn
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubTableColumnAttribute"/> class.
        /// </summary>
        /// <param name="regex">The regex.</param>
        public SubTableColumnAttribute(string regex)
        {
            Regex = regex;
        }

        /// <summary>
        /// Gets the regex to match.
        /// </summary>
        public string Regex { get; }

        /// <summary>
        /// Gets or sets the occurrence to map, or 0 to identify that there should only be a single occurrence.
        /// </summary>
        public int Occurrence { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to ignore case in the regex.
        /// </summary>
        public bool IgnoreCase { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ISubTableColumn" /> is required to be matched.
        /// </summary>
        public bool Required { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating whether to anchor the <see cref="Regex" />.
        /// </summary>
        public bool Anchor { get; set; } = true;
    }
}
