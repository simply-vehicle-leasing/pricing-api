﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Pricing.Services.Tabular
{
    /// <summary>
    /// Utility class for constructing instances of <typeparamref name="TModel"/> from <see cref="DataRow"/>.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    internal class DataTableFactory<TModel> where TModel : class, new()
    {
        private readonly Func<DataRow, TypeMapperSet, TModel> _factoryFunction;
        private readonly TypeMapperSet _mapper;
        private readonly IDictionary<int, string> _expectedColumns;

        public DataTableFactory(Func<DataRow, TypeMapperSet, TModel> factoryFunction, TypeMapperSet mapper, IDictionary<int, string> expectedColumns)
        {
            _factoryFunction = factoryFunction;
            _expectedColumns = expectedColumns;
            _mapper = mapper;
        }

        /// <summary>
        /// Builds a new model from the specified row.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        public TModel Build(DataRow row) => _factoryFunction(row, _mapper);

        /// <summary>
        /// Checks that the specified header row confirms to the schema required by this factory.
        /// </summary>
        /// <param name="headerRow">The header row.</param>
        /// <returns></returns>
        public bool CheckSchema(DataRow headerRow) =>
            headerRow.ItemArray.Length >= _expectedColumns.Keys.Max() + 1
            && _expectedColumns.All(kvp => headerRow.ItemArray[kvp.Key].ToString().Trim().ToUpper() == kvp.Value);
    }
}
