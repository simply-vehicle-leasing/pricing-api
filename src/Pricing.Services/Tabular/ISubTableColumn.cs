﻿namespace Pricing.Services.Tabular
{
    internal interface ISubTableColumn
    {
        /// <summary>
        /// Gets the regex to match.
        /// </summary>
        string Regex { get; }

        /// <summary>
        /// Gets or sets the occurrence to map, or 0 to identify that there should only be a single occurrence.
        /// </summary>
        int Occurrence { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to ignore case in the regex.
        /// </summary>
        bool IgnoreCase { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ISubTableColumn"/> is required to be matched.
        /// </summary>
        bool Required { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to anchor the <see cref="Regex"/>.
        /// </summary>
        bool Anchor { get; set; }
    }
}