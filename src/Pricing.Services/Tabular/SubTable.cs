﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using Pricing.Services.Extensions;

namespace Pricing.Services.Tabular
{
    /// <inheritdoc />
    /// <summary>
    /// Defines a sub-section of a <see cref="DataTable" />
    /// with header rows mapped from <see cref="SubTableColumnAttribute" /> or <see cref="DataTableFactoryBuilder{TModel}"/>.
    /// and value rows mapped to instances of <typeparamref name="TModel" />.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    internal class SubTable<TModel> : IEnumerable<SubTableRow<TModel>> where TModel : class, new()
    {
        private const int DefaultDecimalPrecision = 4;
        private const string DefaultDateFormat = "yyyy-MM-dd";

        private readonly DataTable _table;
        private readonly IList<Func<object, bool>> _rowFilters;
        private readonly IList<Action<DataTableFactoryBuilder<TModel>>> _builders;
        private readonly IList<(PropertyInfo value, ISubTableColumn meta)> _properties;
        private Regex _primaryColumnRegex;
        private int _decimalPrecision = DefaultDecimalPrecision;
        private string _dateFormat = DefaultDateFormat;

        public SubTable(DataTable table)
        {
            _table = table;
            _rowFilters = new List<Func<object, bool>>();
            _builders = new List<Action<DataTableFactoryBuilder<TModel>>>();

            _properties = typeof(TModel).GetProperties()
                                        .Select(p => (value: p, meta: p.GetCustomAttribute<SubTableColumnAttribute>() as ISubTableColumn))
                                        .Where(x => x.meta != null)
                                        .ToList();

            var missingSetMethods = _properties.Where(p => p.value.SetMethod == null).Select(x => x.value.Name).ToList();
            if (missingSetMethods.Any())
            {
                throw new ArgumentException($"Cannot set mapped properties: {string.Join(", ", missingSetMethods)}");
            }
        }

        public SubTable<TModel> PrimaryColumn<TProperty>(Expression<Func<TModel, TProperty>> property)
        {
            var meta = property.GetPropertyInfo().GetCustomAttribute<SubTableColumnAttribute>() ?? throw new ArgumentException("Must map the primary column before defining it");
            _primaryColumnRegex = meta.GetRegex();
            return this;
        }

        public SubTable<TModel> DecimalPrecision(int value)
        {
            _decimalPrecision = value;
            return this;
        }

        public SubTable<TModel> DateFormat(string value)
        {
            _dateFormat = value;
            return this;
        }

        /// <summary>
        /// Filters the rows of the subtable according to the given predicate, which is applied to the raw primary column value, before type conversion
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public SubTable<TModel> Filter(Func<object, bool> filter)
        {
            _rowFilters.Add(filter);
            return this;
        }

        public SubTable<TModel> WithTypeMapper<TMapper>() where TMapper : class, ITypeMapper, new() =>
            With(f => f.WithTypeMapper<TMapper>());

        public SubTable<TModel> WithTypeMapper(ITypeMapper mapper) =>
            With(f => f.WithTypeMapper(mapper));

        public SubTable<TModel> Map<TProperty>(string columnRegex, Expression<Func<TModel, TProperty>> propertyExpression, int occurrence = 0, bool required = true, bool ignoreCase = true)
        {
            var meta = new SubTableColumnAttribute(columnRegex)
                       {
                           Required = required,
                           Occurrence = occurrence,
                           IgnoreCase = ignoreCase
                       };
            _properties.Add((propertyExpression.GetPropertyInfo(), meta));
            return With(f => f.Map(meta, propertyExpression));
        }

        public SubTable<TModel> With(Action<DataTableFactoryBuilder<TModel>> builder)
        {
            _builders.Add(builder);
            return this;
        }

        public IEnumerator<SubTableRow<TModel>> GetEnumerator()
        {
            var headerRow = GetHeaderRow(out var primaryColumnIndex);

            var factoryBuilder = new DataTableFactoryBuilder<TModel>(headerRow, _decimalPrecision, _dateFormat);
            
            foreach (var (value, meta) in _properties)
            {
                factoryBuilder.Map(meta, value);
            }

            foreach (var builder in _builders)
            {
                builder(factoryBuilder);
            }

            var factory = factoryBuilder.Build();
            
            var dataRows = _table.Rows.Cast<DataRow>()
                                 .Select((r, i) => (row: r, index: i))
                                 .Skip(_table.Rows.IndexOf(headerRow) + 1)
                                 .SkipWhile(r => r.row.IsNull(primaryColumnIndex)) // skip until data starts
                                 .TakeWhile(r => !r.row.IsNull(primaryColumnIndex)); // take all data

            return _rowFilters
                  .Aggregate(dataRows, (rows, func) => rows.Where(r => func(r.row.ItemArray[primaryColumnIndex])))
                  .Select(r => new SubTableRow<TModel>()
                               {
                                   RowNumber = r.index + 1,
                                   Data = factory.Build(r.row)
                               })
                  .GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private DataRow GetHeaderRow(out int primaryColumnIndex)
        {
            foreach (var row in _table.Rows.Cast<DataRow>())
            {
                if (!row.ItemArray.Any())
                {
                    continue;
                }

                if (_primaryColumnRegex == null)
                {
                    primaryColumnIndex = 0;
                    if (!row.IsNull(primaryColumnIndex))
                    {
                        return row;
                    }
                }
                else
                {
                    for (var i = 0; i < row.ItemArray.Length; i++)
                    {
                        if (!row.IsNull(i) && _primaryColumnRegex.IsMatch(row.ItemArray[i].ToString()))
                        {
                            primaryColumnIndex = i;
                            return row;
                        }
                    }
                }
            }

            throw new ArgumentException("Cannot find the header row");
        }
    }
}
