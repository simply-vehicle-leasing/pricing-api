﻿namespace Pricing.Services.Tabular
{
    public class SubTableRow<TModel>
    {
        /// <summary>
        /// Gets or sets the index of the row.
        /// </summary>
        public int RowNumber { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public TModel Data { get; set; }
    }
}
