﻿using AutoMapper;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities.Vehicles;
using Pricing.Services.Models.Vehicle;

namespace Pricing.Services.Mapping
{
    /// <summary>
    /// AutoMapper profile for <see cref="Vehicle"/>.
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class VehicleProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleProfile" /> class.
        /// </summary>
        public VehicleProfile()
        {
            CreateMap<Vehicle, VehicleSummaryDto>();
            CreateMap<Vehicle, VehicleDto>();
            CreateMap<VehicleVariant, VehicleVariantDto>().ReverseMap();

            CreateMap<CreateVehicleVariantRequest, VehicleVariant>()
               .ForMember(x => x.Name, o => o.MapFrom(x => x.Variant));
            
            CreateMap<VehicleSearchRequest, GetPaginatedVehicleCollectionRequest>();
        }
    }
}
