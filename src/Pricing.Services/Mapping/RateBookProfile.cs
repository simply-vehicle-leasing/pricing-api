﻿using System.Linq;
using AutoMapper;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities.Rates;
using Pricing.Services.Models.RateBook;

namespace Pricing.Services.Mapping
{
    /// <summary>
    /// An AutoMapper profile for <see cref="RateBook"/>
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class RateBookProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateBookProfile" /> class.
        /// </summary>
        public RateBookProfile()
        {
            CreateMap<CreateRateBookRequest, RateBook>()
                .ForMember(x => x.InvalidRowCount, o => o.MapFrom(x => x.InvalidRows.Count));

            CreateMap<RateBook, RateBookDto>()
               .ForMember(x => x.InvalidRows, o => o.MapFrom(x => x.InvalidRows.OrderBy(r => r.RowNumber)));
            CreateMap<RateBook, RateBookSummaryDto>();

            CreateMap<RateBookSearchRequest, GetPaginatedRateBookCollectionRequest>();

            CreateMap<InvalidRateBookRow, InvalidRateBookRowDto>().ReverseMap();
        }
    }
}
