﻿using AutoMapper;
using Pricing.Data.Entities.Rates;
using Pricing.Services.Models.Rate;

namespace Pricing.Services.Mapping
{
    /// <summary>
    /// An AutoMapper profile for <see cref="Rate"/>
    /// </summary>
    /// <seealso cref="Profile" />
    public class RateProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateProfile" /> class.
        /// </summary>
        public RateProfile()
        {
            CreateMap<CreateRateRequest, Rate>();
            CreateMap<Rate, RateDto>();
        }
    }
}
