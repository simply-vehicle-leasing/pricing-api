﻿using AutoMapper;
using Pricing.Data.Contracts;
using Pricing.Data.Models;
using Pricing.Services.Models;

namespace Pricing.Services.Mapping
{
    /// <summary>
    /// AutoMapper profile for <see cref="PaginatedCollection{TModel}"/>
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class PaginatedCollectionProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaginatedCollectionProfile" /> class.
        /// </summary>
        public PaginatedCollectionProfile()
        {
            CreateMap(typeof(PaginatedCollection<>), typeof(PaginatedCollectionDto<>));
        }
    }
}
