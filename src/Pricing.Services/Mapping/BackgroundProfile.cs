﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Pricing.Data.Entities.Background;
using Pricing.Services.Models.Background;

namespace Pricing.Services.Mapping
{
    /// <summary>
    /// AutoMapper profile for background queue work items.
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class BackgroundProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BackgroundProfile" /> class.
        /// </summary>
        public BackgroundProfile()
        {
            CreateMap<BackgroundQueueWorkItem, BackgroundQueueWorkItemDto>();
        }
    }
}
