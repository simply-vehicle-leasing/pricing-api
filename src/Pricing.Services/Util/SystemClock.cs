﻿using System;
using Pricing.Services.Contracts;

namespace Pricing.Services.Util
{
    public class SystemClock : ISystemClock
    {
        /// <summary>
        /// Gets the current UTC date time.
        /// </summary>
        public DateTimeOffset UtcNow => DateTimeOffset.UtcNow;
    }
}
