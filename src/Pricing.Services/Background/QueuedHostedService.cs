﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Pricing.Data.Contracts;
using Pricing.Data.Entities.Background;

namespace Pricing.Services.Background
{
    /// <summary>
    /// A background task queue.
    /// </summary>
    /// <typeparam name="TWork">The type of the work.</typeparam>
    /// <seealso cref="IHostedService" />
    public abstract class QueuedHostedService<TWork> : IHostedService
    {
        private readonly ConcurrentQueue<(TWork work, int id)> _workItems = new ConcurrentQueue<(TWork work, int id)>();
        private readonly SemaphoreSlim _signal = new SemaphoreSlim(0);
        private readonly CancellationTokenSource _shutdown = new CancellationTokenSource();
        private readonly IServiceProvider _serviceProvider;
        private Task _backgroundTask;



        /// <summary>
        /// The logger
        /// </summary>
        protected readonly ILogger Logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="QueuedHostedService{TWork}" /> class.
        /// </summary>
        /// <param name="loggerFactory">The logger factory.</param>
        /// <param name="serviceProvider">The service provider.</param>
        protected QueuedHostedService(ILoggerFactory loggerFactory, IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            Logger = loggerFactory.CreateLogger(GetType());
        }

        /// <summary>
        /// Triggered when the application host is ready to start the service.
        /// </summary>
        /// <param name="cancellationToken">Indicates that the start process has been aborted.</param>
        /// <returns></returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation("Queued Hosted Service is starting.");

            _backgroundTask = Task.Run(BackgroundProcessing, cancellationToken);

            return Task.CompletedTask;
        }
        
        /// <summary>
        /// Triggered when the application host is performing a graceful shutdown.
        /// </summary>
        /// <param name="cancellationToken">Indicates that the shutdown process should no longer be graceful.</param>
        /// <returns></returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation("Queued Hosted Service is stopping.");

            _shutdown.Cancel();

            return Task.WhenAny(_backgroundTask, Task.Delay(TimeSpan.FromMinutes(1), cancellationToken));
        }

        /// <summary>
        /// Executes the specified work item.
        /// </summary>
        /// <param name="scope">The scope.</param>
        /// <param name="workItem">The work item.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        protected abstract Task DoWorkAsync(IServiceScope scope, TWork workItem, CancellationToken cancellationToken);

        /// <summary>
        /// Queues the specified work item.
        /// </summary>
        /// <param name="work">The work item.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        protected BackgroundQueueWorkItem Queue(TWork work, BackgroundQueueWorkItem item)
        {
            if (work == null)
            {
                throw new ArgumentNullException(nameof(work));
            }

            if (item == null || item.Id == default)
            {
                throw new ArgumentNullException(nameof(item));
            }

            _workItems.Enqueue((work, item.Id));
            
            _signal.Release();

            return item;
        }

        private async Task BackgroundProcessing()
        {
            while (!_shutdown.IsCancellationRequested)
            {
                await _signal.WaitAsync(_shutdown.Token);
                _workItems.TryDequeue(out var workItem);

                _shutdown.Token.ThrowIfCancellationRequested();

                var item = await GetInScopeAsync(workItem.id);
                if (item == null)
                {
                    Logger.LogError($"Error occurred executing {nameof(workItem)}. Cannot find queued item with id = {workItem.id}");
                    continue;
                }
                
                try
                {
                    item.Executed = DateTime.UtcNow;
                    await UpdateInScopeAsync(item);

                    using (var scope = _serviceProvider.CreateScope())
                    {
                        await DoWorkAsync(scope, workItem.work, _shutdown.Token);
                    }

                    item.Completed = DateTime.UtcNow;
                    await UpdateInScopeAsync(item);
                }
                catch (Exception e)
                {
                    var message = $"Error occurred executing {typeof(TWork).Name}.";
                    if (e.InnerException != null && e.InnerException is MySqlException me)
                    {
                        Logger.LogError(me, message);
                        item.Error = me.Message;
                    }
                    else
                    {
                        Logger.LogError(e, message);
                        item.Error = e.Message;
                    }
                    
                    await UpdateInScopeAsync(item);
                }
                
            }
        }

        private async Task UpdateInScopeAsync(BackgroundQueueWorkItem item)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var repository = scope.ServiceProvider.GetRequiredService<IBackgroundQueueWorkItemRepository>();

                try
                {
                    await repository.UpdateAsync(item);
                }
                catch (Exception e)
                {
                    var message = $"Error occurred executing updating work item with id = {item.Id}.";
                    if (e.InnerException != null && e.InnerException is MySqlException me)
                    {
                        Logger.LogError(me, message);
                    }
                    else
                    {
                        Logger.LogError(e, message);
                    }
                }
            }
        }

        private async Task<BackgroundQueueWorkItem> GetInScopeAsync(int id)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var repository = scope.ServiceProvider.GetRequiredService<IBackgroundQueueWorkItemRepository>();
                return await repository.GetAsync(id);
            }
        }

    }
}
