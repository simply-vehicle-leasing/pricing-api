﻿using System;
using System.Data.HashFunction.xxHash;
using System.IO;
using System.Text;

namespace Pricing.Services.Parsing.Common
{
    internal static class StreamHashing
    {
        private static readonly IxxHash XxHash = xxHashFactory.Instance.Create(new xxHashConfig { HashSizeInBits = 32 });

        public static int GetHash(this Stream stream)
        {
            if (!stream.CanSeek)
            {
                throw new ArgumentException("Must be able to seek in order to hash", nameof(stream));
            }
            var value = BitConverter.ToInt32(XxHash.ComputeHash(stream).Hash, 0);
            stream.Seek(0, SeekOrigin.Begin);
            return value;
        }

        public static int GetHash(this string s) => BitConverter.ToInt32(XxHash.ComputeHash(Encoding.UTF8.GetBytes(s)).Hash, 0);
    }
}
