﻿namespace Pricing.Services.Parsing.Common
{
    public interface IVehicleRow : IHaveCapCode
    {
        /// <summary>
        /// Gets the manufacturer.
        /// </summary>
        string Manufacturer { get; }

        /// <summary>
        /// Gets the name of the model.
        /// </summary>
        string Model { get; }

        /// <summary>
        /// Gets the variant.
        /// </summary>
        string Variant { get; }

        /// <summary>
        /// Gets the type of the body.
        /// </summary>
        string BodyType { get; }

        /// <summary>
        /// Gets or sets the P11D price.
        /// </summary>
        decimal? P11D { get; set; }
    }
}