﻿namespace Pricing.Services.Parsing.Common
{
    public struct RentalCost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RentalCost"/> struct.
        /// </summary>
        /// <param name="monthlyPayment">The monthly payment.</param>
        /// <param name="initialPayment">The initial payment.</param>
        /// <param name="totalCost">The total cost.</param>
        public RentalCost(decimal monthlyPayment, decimal initialPayment, decimal totalCost)
        {
            MonthlyPayment = monthlyPayment;
            InitialPayment = initialPayment;
            TotalCost = totalCost;
        }

        /// <summary>
        /// Gets the monthly payment.
        /// </summary>
        public decimal MonthlyPayment { get; }

        /// <summary>
        /// Gets the initial payment.
        /// </summary>
        public decimal InitialPayment { get; }

        /// <summary>
        /// Gets the total cost.
        /// </summary>
        public decimal TotalCost { get; }
    }
}