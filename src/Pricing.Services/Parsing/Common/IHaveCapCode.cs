﻿namespace Pricing.Services.Parsing.Common
{
    public interface IHaveCapCode
    {
        /// <summary>
        /// Gets or sets the CAP code.
        /// </summary>
        string CapCode { get; }
    }
}