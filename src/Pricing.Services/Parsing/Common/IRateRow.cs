﻿using Pricing.Data.Entities;
using Pricing.Data.Models;

namespace Pricing.Services.Parsing.Common
{
    public interface IRateRow : IHaveCapCode
    {
        /// <summary>
        /// Gets the term.
        /// </summary>
        int Term { get; }

        /// <summary>
        /// Gets the initial payments.
        /// </summary>
        int InitialPayments { get; }

        /// <summary>
        /// Gets the rental.
        /// </summary>
        decimal Rental { get; }

        /// <summary>
        /// Gets the vehicle mileage.
        /// </summary>
        int Mileage { get; }

        /// <summary>
        /// Gets the excess mileage cost.
        /// </summary>
        decimal ExcessMileageCost { get; }

        /// <summary>
        /// Gets the contract subject.
        /// </summary>
        ContractSubject ContractSubject { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this contract is maintained.
        /// </summary>
        bool IsMaintained { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this contract includes tyres.
        /// </summary>
        bool IncludesTyres { get; }

        /// <summary>
        /// Gets a value indicating whether this contract includes recovery.
        /// </summary>
        bool IncludesRecovery { get; }
    }
}
