﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentValidation;
using Pricing.Data.Models;
using Pricing.Services.Contracts;
using Pricing.Services.Models.Rate;
using Pricing.Services.Models.RateBook;
using Pricing.Services.Models.Vehicle;

namespace Pricing.Services.Parsing.Common
{
    public class RateBookFactory
    {
        private static readonly IValidator<IRateRow> RateValidator = new RateRowValidator();
        private static readonly IValidator<IVehicleRow> VehicleValidator = new VehicleRowValidator();

        private readonly ConcurrentDictionary<int, CreateVehicleVariantRequest> _vehicles;
        private readonly ConcurrentBag<CreateRateRequest> _rates;
        private readonly ConcurrentBag<InvalidRateBookRowDto> _invalidRows;



        private readonly int _hash;
        private readonly int _fileSize;
        private readonly string _filename;
        private readonly RateBookProvider _provider;
        private readonly DateTime _dateAdded;

        public RateBookFactory(string filename, Stream stream, RateBookProvider provider, DateTime dateAdded)
        {
            _invalidRows = new ConcurrentBag<InvalidRateBookRowDto>();
            _vehicles = new ConcurrentDictionary<int, CreateVehicleVariantRequest>();
            _rates = new ConcurrentBag<CreateRateRequest>();

            _filename = filename;
            _provider = provider;
            _dateAdded = dateAdded;
            _hash = stream.GetHash();
            _fileSize = (int) stream.Length;
        }

        public ProviderRateBookParserResult Build()
        {
            var rates = _rates.ToList();
            return new ProviderRateBookParserResult
                   {
                       RateBook = new CreateRateBookRequest
                                  {
                                      FileHash = _hash,
                                      Provider = _provider,
                                      DateAdded = _dateAdded,
                                      Filename = _filename,
                                      FileSize = _fileSize,
                                      InvalidRows = _invalidRows.ToList(),
                                      Count = rates.Count
                                  },
                       Vehicles = _vehicles.Values,
                       Rates = rates
                   };
        }

        public void AddWide<TRow>(int rowNumber, TRow row) where TRow : IRateRow, IVehicleRow => Add(rowNumber, row, row);

        public void Add(int rowNumber, IVehicleRow vehicleRow, params IRateRow[] rateRows)
        {
            var errors = rateRows.SelectMany(r => RateValidator.Validate(r).Errors)
                                 .Concat(VehicleValidator.Validate(vehicleRow).Errors)
                                 .Select(x => x.ErrorMessage)
                                 .ToList();
            
            if (errors.Any())
            {
                _invalidRows.Add(new InvalidRateBookRowDto
                                 {
                                     RowNumber = rowNumber,
                                     CapCode = vehicleRow.CapCode,
                                     ValidationErrors = string.Join(", ", errors)
                                 });
                return;
            }
            
            var vehicle = GetVehicle(vehicleRow);
            var rates = rateRows.Select(r => GetRate(r, vehicle));

            foreach (var rate in rates)
            {
                _rates.Add(rate);
            }
        }

        private CreateVehicleVariantRequest GetVehicle(IVehicleRow vehicle)
        {
            var manufacturer = vehicle.Manufacturer.Trim();
            var model = vehicle.Model.Trim();
            var id = $"{manufacturer.Trim()}|{model.Trim()}".ToUpper().GetHash();

            return _vehicles.GetOrAdd(id, _ =>
                                          {
                                              var variant = vehicle.Variant.Trim();
                                              if (variant.StartsWith(manufacturer, StringComparison.OrdinalIgnoreCase))
                                              {
                                                  variant = variant.Substring(manufacturer.Length).Trim();
                                              }

                                              if (variant.StartsWith(model, StringComparison.OrdinalIgnoreCase))
                                              {
                                                  variant = variant.Substring(model.Length).Trim();
                                              }

                                              return new CreateVehicleVariantRequest
                                                     {
                                                         VehicleId = id,
                                                         CapCode = vehicle.CapCode.ToUpper().Trim(),
                                                         Manufacturer = manufacturer,
                                                         Model = model,
                                                         Variant = variant,
                                                         P11D = vehicle.P11D == 0 ? null : vehicle.P11D, // map 0 -> null
                                                         BodyType = vehicle.BodyType
                                                     };
                                          });


        }

        private CreateRateRequest GetRate(IRateRow rate, CreateVehicleVariantRequest vehicle)
        {
            decimal RoundPrice(decimal price) => Math.Ceiling(price * 100) / 100;

            var totalCost = (rate.Term + rate.InitialPayments) * rate.Rental;
            var initialPayment = rate.InitialPayments * rate.Rental;
            var monthlyPayment = rate.Rental;
            var cost = new RentalCost(RoundPrice(monthlyPayment), RoundPrice(initialPayment), RoundPrice(totalCost));

            var features = ContractFeatures.None;
            if (rate.IsMaintained)
            {
                features |= ContractFeatures.Maintained;
            }

            if (rate.IncludesTyres)
            {
                features |= ContractFeatures.Tyres;
            }

            if (rate.IncludesRecovery)
            {
                features |= ContractFeatures.Recovery;
            }

            var score = vehicle.P11D.HasValue && vehicle.P11D > 0
                            ? (int) Math.Round(10000 * cost.TotalCost / vehicle.P11D.Value) as int?
                            : null;

            return new CreateRateRequest
                   {
                       CapCode = vehicle.CapCode,
                       VehicleId = vehicle.VehicleId,
                       Mileage = rate.Mileage,
                       InitialPayments = (short) rate.InitialPayments,
                       Term = (short) rate.Term,
                       ExcessMileageCost = rate.ExcessMileageCost,
                       InitialPayment = cost.InitialPayment,
                       MonthlyPayment = cost.MonthlyPayment,
                       TotalCost = cost.TotalCost,
                       ContractSubject = rate.ContractSubject,
                       ContractFeatures = features,
                       ValueScore = score,
                       Provider = _provider
                   };
        }
        
        private class RateRowValidator : AbstractValidator<IRateRow>
        {
            public RateRowValidator()
            {
                RuleFor(x => x.CapCode).NotEmpty();
                RuleFor(x => x.Term).GreaterThan(0);
                RuleFor(x => x.Mileage).GreaterThan(0);
                RuleFor(x => x.InitialPayments).GreaterThan(0);
                RuleFor(x => x.Rental).GreaterThan(0);
                RuleFor(x => x.ExcessMileageCost).GreaterThanOrEqualTo(0);
                RuleFor(x => x.ContractSubject).IsInEnum();
            }
        }

        private class VehicleRowValidator : AbstractValidator<IVehicleRow>
        {
            public VehicleRowValidator()
            {
                RuleFor(x => x.CapCode).NotEmpty();
                RuleFor(x => x.Manufacturer).NotEmpty();
                RuleFor(x => x.Model).NotEmpty();
                RuleFor(x => x.Variant).NotEmpty();
                RuleFor(x => x.BodyType).NotEmpty();
            }
        }
    }
}
