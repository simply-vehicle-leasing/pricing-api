﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Pricing.Data.Models;
using Pricing.Services.Contracts;
using Pricing.Services.Contracts.Exceptions;
using Pricing.Services.Parsing.Common;
using Pricing.Services.Tabular;
using Pricing.Services.Extensions;

namespace Pricing.Services.Parsing
{
    public class LexAutoleaseRateBookParser : IProviderRateBookParser
    {
        private readonly ISystemClock _clock;

        /// <summary>
        /// Initializes a new instance of the <see cref="LexAutoleaseRateBookParser" /> class.
        /// </summary>
        /// <param name="clock">The clock.</param>
        public LexAutoleaseRateBookParser(ISystemClock clock)
        {
            _clock = clock;
        }

        /// <summary>
        /// Determines whether the specified filename is valid for parsing with this parser.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>
        /// <c>true</c> if the specified filename is valid for parsing with this parser; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValidFilename(string filename) => ExcelFilenameValidator.IsValid(filename);

        /// <summary>
        /// Gets the provider.
        /// </summary>
        public RateBookProvider Provider { get; } = RateBookProvider.LexAutolease;

        /// <summary>
        /// Parses all records in the specified content stream with associated filename.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public Task<ProviderRateBookParserResult> ParseRateBookAsync(string filename, Stream stream, CancellationToken cancellationToken)
        {
            try
            {
                var factory = new RateBookFactory(filename, stream, Provider, _clock.UtcNow.UtcDateTime);
                using (var data = new ExcelDataSet(filename, stream))
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    var sheet = data.Tables.GetFirst();

                    sheet.SubTable<RateBookRow>()
                         .PrimaryColumn(x => x.CapCode)
                         .AsParallel()
                         .ForAll(r => factory.AddWide(r.RowNumber, r.Data));

                    cancellationToken.ThrowIfCancellationRequested();
                    
                    return Task.FromResult(factory.Build());
                }
            }
            catch (Exception e)
            {
                throw new ParseException(filename, e);
            }
        }
        
        private class RateBookRow : IRateRow, IVehicleRow
        {
            private static readonly Regex InitialPaymentsRegex = new Regex(@"(\d)\s*down", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            [SubTableColumn(@"Cap\s*Code")]
            public string CapCode { get; set; }

            [SubTableColumn("Manufacture")]
            public string Manufacturer { get; set; }

            [SubTableColumn(@"Model\s*Name")]
            public string Model { get; set; }

            [SubTableColumn("Variant")]
            public string Variant { get; set; }

            [SubTableColumn(@"Body\s*Style")]
            public string BodyType { get; set; }

            [SubTableColumn("Term")]
            public int Term { get; set; }

            [SubTableColumn("P11D")]
            public decimal? P11D { get; set; }

            [SubTableColumn("Mileage")]
            public int Mileage { get; set; }

            [SubTableColumn("Rental")]
            public decimal Rental { get; set; }

            [SubTableColumn(@"Excess\s*Mileage")]
            public decimal ExcessMileageCost { get; set; }

            public ContractSubject ContractSubject { get; } = ContractSubject.Business | ContractSubject.Personal;

            public bool IsMaintained { get; } = false;

            public bool IncludesTyres { get; } = false;

            public bool IncludesRecovery { get; } = false;

            [SubTableColumn(@"Payment\s*Plan")]
            public string PaymentPlan { get; set; }

            public int InitialPayments => InitialPaymentsRegex.TryMatchFirstGroup(PaymentPlan, out var result) && int.TryParse(result, out var payments)
                                              ? payments
                                              : throw new ArgumentException("Cannot find initial payments in payment plan " + PaymentPlan);
        }
    }
}
