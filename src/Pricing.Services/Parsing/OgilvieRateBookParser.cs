﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Pricing.Data.Contracts;
using Pricing.Data.Models;
using Pricing.Services.Contracts;
using Pricing.Services.Contracts.Exceptions;
using Pricing.Services.Parsing.Common;
using Pricing.Services.Tabular;
using Pricing.Services.Extensions;

namespace Pricing.Services.Parsing
{
    public class OgilvieRateBookParser : IProviderRateBookParser
    {
        private readonly ISystemClock _clock;
        private readonly IVehicleRepository _vehicleRepository;
        private readonly ILogger<OgilvieRateBookParser> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="OgilvieRateBookParser" /> class.
        /// </summary>
        /// <param name="clock">The clock.</param>
        /// <param name="vehicleRepository">The vehicle repository.</param>
        /// <param name="logger">The logger.</param>
        public OgilvieRateBookParser(ISystemClock clock, IVehicleRepository vehicleRepository, ILogger<OgilvieRateBookParser> logger)
        {
            _clock = clock;
            _vehicleRepository = vehicleRepository;
            _logger = logger;
        }

        /// <summary>
        /// Determines whether the specified filename is valid for parsing with this parser.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>
        /// <c>true</c> if the specified filename is valid for parsing with this parser; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValidFilename(string filename) => ExcelFilenameValidator.IsValid(filename);

        /// <summary>
        /// Gets the provider.
        /// </summary>
        public RateBookProvider Provider { get; } = RateBookProvider.Ogilvie;

        /// <summary>
        /// Parses all records in the specified content stream with associated filename.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<ProviderRateBookParserResult> ParseRateBookAsync(string filename, Stream stream, CancellationToken cancellationToken)
        {
            var manufacturerLookup = await _vehicleRepository.GetManufacturerModelDictionaryAsync(cancellationToken);

            try
            {
                var factory = new RateBookFactory(filename, stream, Provider, _clock.UtcNow.UtcDateTime);
                using (var data = new ExcelDataSet(filename, stream))
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    var sheet = data.Tables.GetFirst();

                    var paymentMatch = sheet.RegexSearch(@"Payment\s*Plan:?\s*(\d+)");
                    if (paymentMatch == null || !int.TryParse(paymentMatch.Match.Groups[1].Value, out var initialPayments))
                    {
                        throw new ArgumentException("Cannot find payment plan");
                    }

                    var includesRecovery = sheet.RegexSearch(@"Recovery\s*Club:?\s*Roadside\s*Recovery")?.Match.Success ?? false;

                    sheet.SubTable<RateBookRow>()
                         .PrimaryColumn(x => x.CapCode)
                         .AsParallel()
                         .ForAll(row =>
                                 {
                                     var rowData = row.Data;

                                     rowData.InitialPayments = initialPayments;
                                     rowData.IncludesRecovery = includesRecovery;

                                     var makeModelAndVariant = rowData.MakeAndModel.Trim();
                                     var modelLookup = manufacturerLookup.FirstOrDefault(x => makeModelAndVariant.StartsWith(x.Key, StringComparison.OrdinalIgnoreCase));
                                     if (modelLookup.Key == null)
                                     {
                                         _logger.LogError("Cannot find manufacturer: " + makeModelAndVariant);
                                         return;
                                     }

                                     rowData.Manufacturer = modelLookup.Key;
                                     var modelAndVariant = makeModelAndVariant.Substring(modelLookup.Key.Length).Trim();

                                     rowData.Model = modelLookup.Value.FirstOrDefault(x => x.StartsWith(x, StringComparison.OrdinalIgnoreCase));
                                     if (rowData.Model == null)
                                     {
                                         _logger.LogError("Cannot find model: " + modelAndVariant);
                                         return;
                                     }

                                     rowData.Variant = modelAndVariant.Substring(rowData.Model.Length).Trim();

                                     factory.AddWide(row.RowNumber, rowData);
                                 });

                    cancellationToken.ThrowIfCancellationRequested();
                    
                    return factory.Build();
                }
            }
            catch (Exception e)
            {
                throw new ParseException(filename, e);
            }
        }

        private class RateBookRow : IRateRow, IVehicleRow
        {
            private static readonly Regex MaintenanceRegex = new Regex(@"(?<!No)\s*Maintenance", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            private static readonly Regex TyresRegex = new Regex("Tyres", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            [SubTableColumn(@"CAP\s*Code")]
            public string CapCode { get; set; }

            public string Manufacturer { get; set; }

            public string Model { get; set; }

            public string Variant { get; set; }

            [SubTableColumn(@"Body\s*Type")]
            public string BodyType { get; set; }

            [SubTableColumn("Term")]
            public int Term { get; set; }

            public int InitialPayments { get; set; }

            [SubTableColumn(@"Cost\s*For\s*Tax")]
            public decimal? P11D { get; set; }

            [SubTableColumn("Distance")]
            public int Mileage { get; set; }

            [SubTableColumn(@"Total\s*Rental")]
            public decimal Rental { get; set; }

            [SubTableColumn(@"Excess\s+.*")]
            public decimal ExcessMileageCost { get; set; }

            public ContractSubject ContractSubject { get; } = ContractSubject.Business | ContractSubject.Personal;

            public bool IsMaintained => MaintenanceRegex.IsMatch(Product);

            public bool IncludesTyres => TyresRegex.IsMatch(Product);

            public bool IncludesRecovery { get; set; }

            [SubTableColumn(@"Make\s*Model\s*Name")]
            public string MakeAndModel { get; set; }

            [SubTableColumn("Product")]
            public string Product { get; set; }
        }
    }
}
