﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Pricing.Data.Models;
using Pricing.Services.Contracts;
using Pricing.Services.Contracts.Exceptions;
using Pricing.Services.Parsing.Common;
using Pricing.Services.Tabular;
using Pricing.Services.Extensions;

namespace Pricing.Services.Parsing
{
    public class HitachiRateBookParser : IProviderRateBookParser
    {
        private const int DefaultInitialPayments = 3;
        private readonly ISystemClock _clock;

        /// <summary>
        /// Initializes a new instance of the <see cref="HitachiRateBookParser"/> class.
        /// </summary>
        /// <param name="clock">The clock.</param>
        public HitachiRateBookParser(ISystemClock clock)
        {
            _clock = clock;
        }

        /// <summary>
        /// Determines whether the specified filename is valid for parsing with this parser.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>
        /// <c>true</c> if the specified filename is valid for parsing with this parser; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValidFilename(string filename) => ExcelFilenameValidator.IsValid(filename);

        /// <summary>
        /// Gets the provider.
        /// </summary>
        public RateBookProvider Provider { get; } = RateBookProvider.Hitachi;

        /// <summary>
        /// Parses the rate book.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public Task<ProviderRateBookParserResult> ParseRateBookAsync(string filename, Stream stream, CancellationToken cancellationToken)
        {
            try
            {
                var factory = new RateBookFactory(filename, stream, Provider, _clock.UtcNow.UtcDateTime);
                using (var data = new ExcelDataSet(filename, stream))
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    var sheet = data.Tables.GetFirst();
                    sheet.SubTable<RateBookRow>()
                         .PrimaryColumn(x => x.CapCode)
                         .AsParallel()
                         .ForAll(row => factory.Add(row.RowNumber, row.Data, row.Data.Rates.ToArray()));

                    cancellationToken.ThrowIfCancellationRequested();

                    return Task.FromResult(factory.Build());
                }
            }
            catch (Exception e)
            {
                throw new ParseException(filename, e);
            }
        }

        private class RateBookRow : IVehicleRow
        {
            private const string TermRegex = "POL";
            private const string RentalRegex = @"Finance\s*Rental";
            private const string MileageRegex = "Mileage";
            private const string ExcessMileageCostRegex = @"Excess\s*Rate";

            [SubTableColumn(@"CAP\s*Code")]
            public string CapCode { get; set; }

            [SubTableColumn("Make")]
            public string Manufacturer { get; set; }

            [SubTableColumn("Model")]
            public string Model { get; set; }

            [SubTableColumn("Variant.*Description")]
            public string Variant { get; set; }

            [SubTableColumn("Body")]
            public string BodyType { get; set; }

            [SubTableColumn("P11D.*")]
            public decimal? P11D { get; set; }

            [SubTableColumn(TermRegex, Occurrence = 1)]
            public int Term1 { get; set; }

            [SubTableColumn(RentalRegex, Occurrence = 1)]
            public decimal Rental1 { get; set; }

            [SubTableColumn(MileageRegex, Occurrence = 1)]
            public int Mileage1 { get; set; }

            [SubTableColumn(ExcessMileageCostRegex, Occurrence = 1)]
            public decimal ExcessMileageCost1 { get; set; }

            [SubTableColumn(TermRegex, Occurrence = 2, Required = false)]
            public int Term2 { get; set; }

            [SubTableColumn(RentalRegex, Occurrence = 2, Required = false)]
            public decimal Rental2 { get; set; }

            [SubTableColumn(MileageRegex, Occurrence = 2, Required = false)]
            public int Mileage2 { get; set; }

            [SubTableColumn(ExcessMileageCostRegex, Occurrence = 2, Required = false)]
            public decimal ExcessMileageCost2 { get; set; }

            public IEnumerable<IRateRow> Rates {
                get
                {
                    yield return new RateRow(CapCode, Term1, Rental1, Mileage1, ExcessMileageCost1);
                    if (Term2 > 0)
                    {
                        yield return new RateRow(CapCode, Term2, Rental2, Mileage2, ExcessMileageCost2);
                    }
                }
            }
        }

        private class RateRow : IRateRow
        {
            public RateRow(string capCode, int term, decimal rental, int mileage, decimal excessMileageCost)
            {
                CapCode = capCode;
                Term = term;
                Rental = rental;
                Mileage = mileage;
                ExcessMileageCost = excessMileageCost;
            }

            public string CapCode { get; }

            public int Term { get; }

            public int InitialPayments { get; } = DefaultInitialPayments;

            public decimal Rental { get; }

            public int Mileage { get; }

            public decimal ExcessMileageCost { get; }

            public ContractSubject ContractSubject { get; } = ContractSubject.Personal | ContractSubject.Business;

            public bool IsMaintained { get; } = false;

            public bool IncludesTyres { get; } = false;

            public bool IncludesRecovery { get; } = false;
        }
    }
}
