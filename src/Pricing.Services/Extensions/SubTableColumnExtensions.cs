﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Pricing.Services.Tabular;

namespace Pricing.Services.Extensions
{
    internal static class SubTableColumnExtensions
    {
        public static Regex GetRegex(this ISubTableColumn column)
        {
            var regexOptions = RegexOptions.Compiled;
            if (column.IgnoreCase)
            {
                regexOptions |= RegexOptions.IgnoreCase;
            }

            var regexString = column.Regex;

            if (column.Anchor)
            {
                // Anchor if not already anchored.
                if (!regexString.StartsWith("^"))
                {
                    regexString = $@"^\s*{regexString}";
                }

                if (!regexString.EndsWith("$"))
                {
                    regexString = $@"{regexString}\s*$";
                }
            }

            return new Regex(regexString, regexOptions);
        }
    }
}
