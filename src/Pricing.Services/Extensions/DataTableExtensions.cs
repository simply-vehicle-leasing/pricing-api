﻿using System;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Pricing.Services.Tabular;

namespace Pricing.Services.Extensions
{
    internal static class DataTableExtensions
    {
        /// <summary>
        /// Reads a titled sub table from the specified <see cref="DataTable" /> and maps it through the specified factory builder to return parse all instances of <typeparamref name="TModel" />.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="table">The table.</param>
        /// <returns></returns>
        public static SubTable<TModel> SubTable<TModel>(this DataTable table)
            where TModel : class, new() => new SubTable<TModel>(table);

        /// <summary>
        /// Searches the specified data table with the specified regex.
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="regexString">The regex string.</param>
        /// <returns></returns>
        public static RegexSearchMatch RegexSearch(this DataTable table, string regexString)
        {
            var regex = new Regex(regexString, RegexOptions.IgnoreCase | RegexOptions.Compiled);
            return table.Rows.Cast<DataRow>()
                .SelectMany((x, row) => x.ItemArray.Select((y, column) => (value: y, row: row, column: column)))
                .Where(x => x.value is string)
                .Select(x =>
                {
                    var match = regex.Match(x.value.ToString().Trim());
                    return match.Success
                        ? new RegexSearchMatch(match, x.row, x.column)
                        : null;
                })
                .FirstOrDefault(x => x != null);
        }

        /// <summary>
        /// Gets the index of the first non empty column in the specified row.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        public static int? GetFirstNonEmptyColumnIndex(this DataRow row)
        {
            var firstItem = row.ItemArray.SkipWhile((x, i) => row.IsNull(i) || string.IsNullOrWhiteSpace(x?.ToString())).FirstOrDefault();
            return firstItem == null ? null : Array.IndexOf(row.ItemArray, firstItem) as int?;
        }

        /// <summary>
        /// Gets a table from the specified collection via regex.
        /// </summary>
        /// <param name="tables">The tables.</param>
        /// <param name="regex">The regex.</param>
        /// <returns></returns>
        public static DataTable Get(this DataTableCollection tables, string regex)
        {
            var rgx = new Regex(regex, RegexOptions.IgnoreCase | RegexOptions.Compiled);
            return tables.Cast<DataTable>().SingleOrDefault(t => rgx.IsMatch(t.TableName))
                   ?? throw new ArgumentException($"Cannot match regex {regex} to tables: {string.Join(", ", tables.Cast<DataTable>().Select(t => t.TableName))}");
        }

        /// <summary>
        /// Gets the first table from the specified collection.
        /// </summary>
        /// <param name="tables">The tables.</param>
        /// <returns></returns>
        public static DataTable GetFirst(this DataTableCollection tables) =>
            tables.Cast<DataTable>().FirstOrDefault() ?? throw new ArgumentException("There are no tables in the data table collection");
    }

    public class RegexSearchMatch
    {
        public RegexSearchMatch(Match match, int row, int column)
        {
            Match = match;
            Row = row;
            Column = column;
        }

        public Match Match { get; }

        public int Row { get; }

        public int Column { get; }
    }
}
