﻿using System.Text.RegularExpressions;

namespace Pricing.Services.Extensions
{
    internal static class RegexExtensions
    {
        public static bool TryMatchFirstGroup(this Regex regex, string input, out string result)
        {
            var match = regex.Match(input);
            if (!match.Success || !match.Groups[1].Success)
            {
                result = null;
                return false;
            }

            result = match.Groups[1].Value;
            return true;
        }
    }
}
