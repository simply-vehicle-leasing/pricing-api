﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using AgileObjects.ReadableExpressions;

namespace Pricing.Services.Extensions
{
    internal static class ExpressionExtensions
    {
        public static PropertyInfo GetPropertyInfo<TEntity, TProperty>(this Expression<Func<TEntity, TProperty>> property)
        {
            switch (property.Body)
            {
                case MemberExpression member:
                    return member.Member as PropertyInfo ?? throw new ArgumentException("Not a property: " + property.ToReadableString(), nameof(property));

                case UnaryExpression unary when unary.Operand is MemberExpression operand:
                    return operand.Member as PropertyInfo ?? throw new ArgumentException("Not a property: " + property.ToReadableString(), nameof(property));

                default:
                    throw new ArgumentException("Not a member or unary expression: " + property.ToReadableString(), nameof(property));
            }
        }

        public static string GetMemberName<TEntity, TProperty>(this Expression<Func<TEntity, TProperty>> property)
        {
            switch (property.Body)
            {
                case MemberExpression member:
                    return member.Member.Name;

                case UnaryExpression unary when unary.Operand is MemberExpression operand:
                    return operand.Member.Name;

                default:
                    throw new ArgumentException("Not a member or unary expression: " + property, nameof(property));
            }
        }
    }
}
