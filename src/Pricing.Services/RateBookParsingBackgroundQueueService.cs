﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Pricing.Data.Entities.Background;
using Pricing.Services.Background;
using Pricing.Services.Contracts;
using Pricing.Services.Models.RateBook;

namespace Pricing.Services
{
    /// <summary>
    /// Service for parsing rate books.
    /// </summary>
    /// <seealso cref="IRateBookParsingBackgroundQueueService" />
    public class RateBookParsingBackgroundQueueService : QueuedHostedService<ParseRateBookRequest>, IRateBookParsingBackgroundQueueService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateBookParsingBackgroundQueueService" /> class.
        /// </summary>
        /// <param name="loggerFactory">The logger factory.</param>
        /// <param name="serviceProvider">The service provider.</param>
        public RateBookParsingBackgroundQueueService(ILoggerFactory loggerFactory, IServiceProvider serviceProvider) : base(loggerFactory, serviceProvider)
        {
        }

        /// <summary>
        /// Parses a rate book according to the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">File</exception>
        public void EnqueueRateBookParsingRequest(ParseRateBookRequest request, BackgroundQueueWorkItem item) => Queue(request, item);

        /// <summary>
        /// Executes the specified work item.
        /// </summary>
        /// <param name="scope">The scope.</param>
        /// <param name="request">The work item.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        protected override async Task DoWorkAsync(IServiceScope scope, ParseRateBookRequest request, CancellationToken cancellationToken)
        {
            var service = scope.ServiceProvider.GetRequiredService<IRateBookParsingService>();
            await service.ParseRateBookAsync(request, cancellationToken);
        }
    }
}
