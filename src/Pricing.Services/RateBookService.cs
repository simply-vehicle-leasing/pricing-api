﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Pricing.Data.Contracts;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Rates;
using Pricing.Services.Contracts;
using Pricing.Services.Contracts.Exceptions;
using Pricing.Services.Crud;
using Pricing.Services.Models.RateBook;

namespace Pricing.Services
{
    /// <summary>
    /// Service for managing rate books.
    /// </summary>
    /// <seealso cref="SearchableEntityFrameworkCrudService{RateBook, IRateBookRepository, RateBookSummaryDto, CreateRateBookRequest, GetPaginatedRateBookCollectionRequest, RateBookSearchRequest}" />
    /// <seealso cref="IRateBookService" />
    public class RateBookService : SearchableEntityFrameworkCrudService<RateBook, IRateBookRepository, RateBookSummaryDto, CreateRateBookRequest, GetPaginatedRateBookCollectionRequest, RateBookSearchRequest>, IRateBookService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateBookService" /> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="httpContextAccessor">The HTTP context accessor.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        public RateBookService(IRateBookRepository repository,
                               IMapper mapper,
                               IHttpContextAccessor httpContextAccessor,
                               ILoggerFactory loggerFactory)
            : base(repository, mapper, httpContextAccessor, loggerFactory)
        {
        }

        /// <summary>
        /// Gets the rate book detail for the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<RateBookDto> GetDetailAsync(int id)
        {
            var rateBook = await Repository.GetWithInvalidRatesAsync(id, RequestAborted);
            if (rateBook == null)
            {
                throw EntityNotFoundException.Create<RateBook>(x => x.Id, id);
            }

            return Mapper.Map<RateBookDto>(rateBook);
        }
    }
}
