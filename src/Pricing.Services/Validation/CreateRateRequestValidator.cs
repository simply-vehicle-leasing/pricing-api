﻿using FluentValidation;
using Pricing.Services.Models.Rate;

namespace Pricing.Services.Validation
{
    /// <summary>
    /// Validator for <see cref="CreateRateRequest"/>.
    /// </summary>
    /// <seealso cref="FluentValidation.AbstractValidator{CreateRateRequest}" />
    public class CreateRateRequestValidator : AbstractValidator<CreateRateRequest>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateRateRequestValidator" /> class.
        /// </summary>
        public CreateRateRequestValidator()
        {
            RuleFor(x => x.VehicleId).NotEmpty();
            RuleFor(x => x.CapCode).NotEmpty();
            RuleFor(x => x.Term).GreaterThan((short)0);
            RuleFor(x => x.Mileage).GreaterThan(0);
            RuleFor(x => x.InitialPayments).GreaterThan((short)0);
            RuleFor(x => x.InitialPayment).GreaterThan(0);
            RuleFor(x => x.MonthlyPayment).GreaterThan(0);
            RuleFor(x => x.TotalCost).GreaterThan(0);
            RuleFor(x => x.ExcessMileageCost).GreaterThanOrEqualTo(0);
            RuleFor(x => x.Provider).IsInEnum();
            RuleFor(x => x.ContractSubject).IsInEnum();
            RuleFor(x => x.ContractFeatures).IsInEnum();
        }
    }

}
