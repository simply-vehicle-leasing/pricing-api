﻿using FluentValidation;
using Pricing.Services.Contracts;
using Pricing.Services.Models.RateBook;
using Pricing.Services.Models.Vehicle;

namespace Pricing.Services.Validation
{
    /// <summary>
    /// Validator for <see cref="ProviderRateBookParserResult"/>.
    /// </summary>
    /// <seealso cref="ProviderRateBookParserResult" />
    public class ProviderRateBookParserResultValidator : AbstractValidator<ProviderRateBookParserResult>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProviderRateBookParserResultValidator" /> class.
        /// </summary>
        public ProviderRateBookParserResultValidator()
        {
            RuleFor(x => x.RateBook).NotNull().SetValidator(new CreateRateBookRequestValidator());
            RuleFor(x => x.Vehicles).NotEmpty().SetCollectionValidator(new CreateVehicleRequestValidator());
            RuleFor(x => x.Rates).NotEmpty().SetCollectionValidator(new CreateRateRequestValidator());
        }

        private class CreateRateBookRequestValidator : AbstractValidator<CreateRateBookRequest>
        {
            public CreateRateBookRequestValidator()
            {
                RuleFor(x => x.Filename).NotEmpty().MaximumLength(255);
                RuleFor(x => x.FileHash).NotEmpty();
                RuleFor(x => x.Provider).IsInEnum();
                RuleFor(x => x.FileSize).GreaterThan(0);
                RuleFor(x => x.DateAdded).NotEmpty();
                RuleFor(x => x.Count).GreaterThan(0);
                RuleFor(x => x.InvalidRows)
                    .Must((request, invalidRows) => (double) invalidRows.Count / (request.Count + invalidRows.Count) < 0.05)
                    .WithMessage("The number of invalid rates must not exceed 5% of the total rate book");
            }
        }

        private class CreateVehicleRequestValidator : AbstractValidator<CreateVehicleVariantRequest>
        {
            public CreateVehicleRequestValidator()
            {
                RuleFor(x => x.VehicleId).NotEmpty();
                RuleFor(x => x.CapCode).NotEmpty();
                RuleFor(x => x.Manufacturer).NotEmpty();
                RuleFor(x => x.Model).NotEmpty();
                RuleFor(x => x.Variant).NotEmpty();
                RuleFor(x => x.P11D).GreaterThanOrEqualTo(0);
            }
        }
    }
}
