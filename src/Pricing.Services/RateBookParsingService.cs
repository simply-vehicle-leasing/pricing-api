﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Pricing.Data.Contracts;
using Pricing.Data.Entities.Background;
using Pricing.Data.Models;
using Pricing.Services.Contracts;
using Pricing.Services.Contracts.Exceptions;
using Pricing.Services.Models.Background;
using Pricing.Services.Models.RateBook;

namespace Pricing.Services
{
    /// <summary>
    /// Service for parsing rate books.
    /// </summary>
    /// <seealso cref="IRateBookParsingService" />
    public class RateBookParsingService : IRateBookParsingService
    {
        private readonly ILogger<RateBookParsingService> _logger;
        private readonly IRateBookService _rateBookService;
        private readonly IVehicleService _vehicleService;
        private readonly IRateService _rateService;
        private readonly IDictionary<RateBookProvider, IProviderRateBookParser> _parsers;
        private readonly IValidatorFactory _validatorFactory;
        private readonly IBackgroundQueueWorkItemRepository _workItemRepository;
        private readonly IRateBookParsingBackgroundQueueService _queueService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="RateBookParsingService" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="rateBookService">The rate book service.</param>
        /// <param name="vehicleService">The vehicle service.</param>
        /// <param name="rateService">The rate service.</param>
        /// <param name="parsers">The parsers.</param>
        /// <param name="validatorFactory">The validator factory.</param>
        /// <param name="workItemRepository">The work item repository.</param>
        /// <param name="queueService">The queue service.</param>
        /// <param name="httpContextAccessor">The HTTP context accessor.</param>
        /// <param name="mapper">The mapper.</param>
        public RateBookParsingService(ILogger<RateBookParsingService> logger,
                                      IRateBookService rateBookService,
                                      IVehicleService vehicleService,
                                      IRateService rateService,
                                      IEnumerable<IProviderRateBookParser> parsers,
                                      IValidatorFactory validatorFactory,
                                      IBackgroundQueueWorkItemRepository workItemRepository,
                                      IRateBookParsingBackgroundQueueService queueService,
                                      IHttpContextAccessor httpContextAccessor,
                                      IMapper mapper)
        {
            _logger = logger;
            _rateBookService = rateBookService;
            _vehicleService = vehicleService;
            _rateService = rateService;
            _parsers = parsers.ToDictionary(x => x.Provider);
            _validatorFactory = validatorFactory;
            _workItemRepository = workItemRepository;
            _queueService = queueService;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
        }

        /// <summary>
        /// Parses a rate book according to the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public async Task<BackgroundQueueWorkItemDto> EnqueueRateBookParsingRequestAsync(EnqueueParseRateBookRequest request)
        {
            if (request.File == null)
            {
                throw new ArgumentNullException(nameof(request.File));
            }

            if (!_parsers.TryGetValue(request.Provider, out _))
            {
                throw new ArgumentException("Cannot find a parser for rate book provider: " + request.Provider, nameof(request.Provider));
            }

            byte[] fileContent;
            using (var stream = new MemoryStream())
            {
                await request.File.CopyToAsync(stream, RequestAborted);
                fileContent = stream.ToArray();
            }

            var workItemRequest = new ParseRateBookRequest
                                  {
                                      Provider = request.Provider,
                                      Filename = request.File.FileName,
                                      FileContent = fileContent
                                  };

            var item = new BackgroundQueueWorkItem
                       {
                           Name = request.File.FileName,
                           Type = BackgroundConstants.RateBookParseType,
                           Queued = DateTime.UtcNow
                       };

            await _workItemRepository.AddAsync(item, RequestAborted);

            _queueService.EnqueueRateBookParsingRequest(workItemRequest, item);

            return _mapper.Map<BackgroundQueueWorkItemDto>(item);
        }

        /// <summary>
        /// Gets all queued rate book parsing requests.
        /// </summary>
        /// <param name="limit">The limit.</param>
        /// <returns></returns>
        public async Task<ICollection<BackgroundQueueWorkItemDto>> GetAllQueuedRateBookParsingRequestsAsync(int limit)
        {
            var items = await _workItemRepository.GetAllWithTypeAsync(BackgroundConstants.RateBookParseType, limit, RequestAborted);
            return _mapper.Map<ICollection<BackgroundQueueWorkItemDto>>(items);
        }

        /// <summary>
        /// Executes the specified work item.
        /// </summary>
        /// <param name="request">The work item.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task ParseRateBookAsync(ParseRateBookRequest request, CancellationToken cancellationToken)
        {
            if (!_parsers.TryGetValue(request.Provider, out var parser))
            {
                throw new ArgumentException("Cannot find a parser for rate book provider: " + request.Provider, nameof(request.Provider));
            }

            _logger.LogInformation($"Parsing new rate book: {request.Filename} for provider: {request.Provider} using {parser.GetType()}");

            ProviderRateBookParserResult result;
            try
            {
                using (var stream = new MemoryStream(request.FileContent))
                {
                    result = await parser.ParseRateBookAsync(request.Filename, stream, cancellationToken);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Failed to parse rate book: {request.Filename} for provider: {request.Provider}");
                throw;
            }

            _logger.LogInformation($"Parsed {request.Filename} for provider: {request.Provider}. " +
                                   $"Found {result.Rates.Count} rates and ignored {result.RateBook.InvalidRows.Count} invalid rows");

            // Validate.
            var validator = _validatorFactory.GetValidator<ProviderRateBookParserResult>();
            var validation = await validator.ValidateAsync(result, cancellationToken);
            if (!validation.IsValid)
            {
                throw new BadRequestException(null, validation.Errors.ToArray());
            }
            
            var profiles = result.Rates.Select(x => new ContractProfile(x.Term, x.Mileage)).Distinct().ToList();
            await _rateService.DeleteByProfilesAsync(request.Provider, profiles);

            await _vehicleService.InsertOrUpdateManyAsync(result.Vehicles);
            await _rateService.CreateManyAsync(result.Rates);
            await _rateBookService.CreateAsync(result.RateBook);
        }

        /// <summary>
        /// Gets the queued rate book parsing request with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<BackgroundQueueWorkItemDto> GetQueuedRateBookParsingRequestAsync(int id)
        {
            var item = await _workItemRepository.GetAsync(id, RequestAborted);
            if (item == null)
            {
                throw EntityNotFoundException.Create<BackgroundQueueWorkItem>(x => x.Id, id);
            }

            return _mapper.Map<BackgroundQueueWorkItemDto>(item);
        }

        private CancellationToken RequestAborted => _httpContextAccessor.HttpContext?.RequestAborted ?? CancellationToken.None;
    }
}