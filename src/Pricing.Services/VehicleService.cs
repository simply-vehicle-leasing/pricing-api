﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Pricing.Data.Contracts;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Vehicles;
using Pricing.Services.Contracts;
using Pricing.Services.Contracts.Exceptions;
using Pricing.Services.Crud;
using Pricing.Services.Models.Vehicle;

namespace Pricing.Services
{
    /// <summary>
    /// /// Service for managing vehicles.
    /// </summary>
    /// <seealso cref="SearchableEntityFrameworkCrudService{Vehicle, IVehicleRepository, VehicleSummaryDto, CreateVehicleVariantRequest, GetPaginatedVehicleCollectionRequest, VehicleSearchRequest}" />
    /// <seealso cref="IVehicleService" />
    public class VehicleService : SearchableEntityFrameworkCrudService<Vehicle, IVehicleRepository, VehicleSummaryDto, CreateVehicleVariantRequest, GetPaginatedVehicleCollectionRequest, VehicleSearchRequest>, IVehicleService
    {
        private static readonly string ModelsKey = $"{nameof(VehicleService)}.{nameof(GetAllModelsAsync)}";
        private static readonly string ManufacturersKey = $"{nameof(VehicleService)}.{nameof(GetAllManufacturersAsync)}";
        private readonly IMemoryCache _memoryCache;
        private readonly IVehicleVariantRepository _vehicleVariantRepository;


        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleService" /> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="httpContextAccessor">The HTTP context accessor.</param>
        /// <param name="memoryCache">The memory cache.</param>
        /// <param name="vehicleVariantRepository">The vehicle variant repository.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        public VehicleService(IVehicleRepository repository,
                              IMapper mapper,
                              IHttpContextAccessor httpContextAccessor,
                              IMemoryCache memoryCache,
                              IVehicleVariantRepository vehicleVariantRepository,
                              ILoggerFactory loggerFactory) : base(repository, mapper, httpContextAccessor, loggerFactory)
        {
            _memoryCache = memoryCache;
            _vehicleVariantRepository = vehicleVariantRepository;
        }

        /// <summary>
        /// Inserts or updates the specified vehicles.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <returns></returns>
        public async Task InsertOrUpdateManyAsync(ICollection<CreateVehicleVariantRequest> requests)
        {
            var invalidateCache = false;

            var existingVehicleIds = await Repository.GetAllIdsAsync(RequestAborted);
            var newVehicleRequests = requests.Where(x => !existingVehicleIds.Contains(x.VehicleId)).ToList();
            if (newVehicleRequests.Any())
            {
                var newVehicles = newVehicleRequests.GroupBy(x => x.VehicleId)
                                                    .Select(grp =>
                                                            {
                                                                var request = grp.First();
                                                                Logger.LogInformation($"Found new vehicle: {request.Manufacturer} {request.Model}");
                                                                return new Vehicle
                                                                       {
                                                                           Id = grp.Key,
                                                                           Model = request.Model,
                                                                           Manufacturer = request.Manufacturer,
                                                                           Variants = Mapper.Map<ICollection<VehicleVariant>>(grp)
                                                                       };
                                                            })
                                                    .ToList();

                await Repository.AddRangeAsync(newVehicles, RequestAborted);
                invalidateCache = true;
            }

            var existingCapCodes = await _vehicleVariantRepository.GetAllCapCodesAsync(RequestAborted);
            var newVariantRequests = requests.Except(newVehicleRequests).Where(x => !existingCapCodes.Contains(x.CapCode)).ToList();

            if (newVariantRequests.Any())
            {
                Logger.LogInformation($"Found {newVariantRequests.Count} new CAP codes");
                var variants = Mapper.Map<ICollection<VehicleVariant>>(newVariantRequests);
                await _vehicleVariantRepository.AddRangeAsync(variants, RequestAborted);
                invalidateCache = true;
            }

            if (invalidateCache)
            {
                // Invalidate the cache.
                _memoryCache.Remove(ModelsKey);
                _memoryCache.Remove(ManufacturersKey);
            }
        }

        /// <summary>
        /// Creates the asynchronous.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public override Task<VehicleSummaryDto> CreateAsync(CreateVehicleVariantRequest request) =>
            throw new NotSupportedException($"Use {nameof(InsertOrUpdateManyAsync)}");

        /// <summary>
        /// Creates the many asynchronous.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <returns></returns>
        public override Task<ICollection<VehicleSummaryDto>> CreateManyAsync(ICollection<CreateVehicleVariantRequest> requests) =>
            throw new NotSupportedException($"Use {nameof(InsertOrUpdateManyAsync)}");

        /// <summary>
        /// Gets all vehicle models.
        /// </summary>
        /// <returns></returns>
        public Task<ICollection<string>> GetAllModelsAsync() =>
            _memoryCache.GetOrCreateAsync(ModelsKey, c => Repository.GetAllModelsAsync(RequestAborted));

        /// <summary>
        /// Gets all vehicle manufacturers.
        /// </summary>
        /// <returns></returns>
        public Task<ICollection<string>> GetAllManufacturersAsync() =>
            _memoryCache.GetOrCreateAsync(ManufacturersKey, c => Repository.GetAllManufacturersAsync(RequestAborted));

        /// <summary>
        /// Gets the <see cref="VehicleDto" /> with the specified identifier, including all variants.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<VehicleDto> GetDetailAsync(int id)
        {
            var vehicle = await Repository.GetWithVariantsAsync(id, RequestAborted);
            if (vehicle == null)
            {
                throw EntityNotFoundException.Create<Vehicle>(x => x.Id, id);
            }

            return Mapper.Map<VehicleDto>(vehicle);
        }
    }
}
