﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Pricing.Api.Infrastructure;
using Pricing.Services.Contracts;
using Pricing.Services.Models.Background;
using Pricing.Services.Models.RateBook;

namespace Pricing.Api.Controllers
{
    [ApiRoute]
    public class RateBookParsingController : Controller
    {
        private readonly IRateBookParsingService _service;

        /// <summary>
        /// Initializes a new instance of the <see cref="RateBookParsingController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public RateBookParsingController(IRateBookParsingService service)
        {
            _service = service;
        }

        /// <summary>
        /// Adds the specified rate book parsing request to the rate book parsing queues.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateModel]
        public Task<BackgroundQueueWorkItemDto> EnqueueRateBookParsingRequest(EnqueueParseRateBookRequest request) => _service.EnqueueRateBookParsingRequestAsync(request);

        /// <summary>
        /// Gets all queued rate book parsing requests.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Task<ICollection<BackgroundQueueWorkItemDto>> GetAllQueuedRateBookParsingRequests(int limit = 10) => _service.GetAllQueuedRateBookParsingRequestsAsync(limit);

        /// <summary>
        /// Gets all queued rate book parsing requests.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public Task<BackgroundQueueWorkItemDto> GetQueuedRateBookParsingRequest(int id) => _service.GetQueuedRateBookParsingRequestAsync(id);

    }
}
