﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Pricing.Api.Infrastructure;
using Pricing.Services.Contracts;
using Pricing.Services.Models.Rate;

namespace Pricing.Api.Controllers
{
    /// <summary>
    /// Rates.
    /// </summary>
    [ApiRoute]
    public class RateController
    {
        private readonly IRateService _service;

        /// <summary>
        /// Initializes a new instance of the <see cref="RateController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public RateController(IRateService service)
        {
            _service = service;
        }

        /// <summary>
        /// Gets all rates by the specified CAP code.
        /// </summary>
        /// <param name="capCode">The cap code.</param>
        /// <returns></returns>
        [HttpGet("by-cap/{capCode}")]
        public Task<ICollection<RateDto>> GetRatesByCapCode(string capCode) => _service.GetByCapCodeAsync(capCode);

        /// <summary>
        /// Gets all rates by the specified vehicle.
        /// </summary>
        /// <param name="vehicleId">The vehicle identifier.</param>
        /// <returns></returns>
        [HttpGet("by-vehicle/{vehicleId}")]
        public Task<ICollection<RateDto>> GetRatesByVehicle(int vehicleId) => _service.GetByVehicleAsync(vehicleId);
    }
}
