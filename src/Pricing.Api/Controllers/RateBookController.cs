﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Pricing.Api.Infrastructure;
using Pricing.Data.Models;
using Pricing.Services.Contracts;
using Pricing.Services.Models;
using Pricing.Services.Models.RateBook;

namespace Pricing.Api.Controllers
{
    /// <summary>
    /// Rate books.
    /// </summary>
    [ApiRoute]
    public class RateBookController : Controller
    {
        private readonly IRateBookService _service;

        /// <summary>
        /// Initializes a new instance of the <see cref="RateBookController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public RateBookController(IRateBookService service)
        {
            _service = service;
        }

        /// <summary>
        /// Gets the rate book detail for the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public Task<RateBookDto> GetRateBookDetail(int id) => _service.GetDetailAsync(id);

        /// <summary>
        /// Searches for rate books according to the specified search request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpGet]
        [ValidateModel]
        public Task<PaginatedCollectionDto<RateBookSummaryDto>> SearchRateBooks([FromQuery] RateBookSearchRequest request) => _service.SearchAsync(request);

        /// <summary>
        /// Gets all rate book providers.
        /// </summary>
        /// <returns></returns>
        [HttpGet("providers")]
        public IDictionary<string, string> GetAllRateBookProviders() => Enum.GetValues(typeof(RateBookProvider))
                                                                            .Cast<RateBookProvider>()
                                                                            .Select(x => typeof(RateBookProvider).GetMember(x.ToString()).First())
                                                                            .ToDictionary(x => x.Name, x => x.GetCustomAttribute<DisplayAttribute>().Name);
    }
}
