﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Pricing.Api.Infrastructure;
using Pricing.Services.Contracts;
using Pricing.Services.Models;
using Pricing.Services.Models.Vehicle;

namespace Pricing.Api.Controllers
{
    /// <summary>
    /// Vehicles.
    /// </summary>
    [ApiRoute]
    public class VehicleController : Controller
    {
        private readonly IVehicleService _service;

        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public VehicleController(IVehicleService service)
        {
            _service = service;
        }

        /// <summary>
        /// Gets the vehicle with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public Task<VehicleDto> GetVehicle(int id) => _service.GetDetailAsync(id);

        /// <summary>
        /// Searches for vehicles according to the specified search request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpGet]
        [ValidateModel]
        public Task<PaginatedCollectionDto<VehicleSummaryDto>> SearchVehicles([FromQuery] VehicleSearchRequest request) => _service.SearchAsync(request);

        /// <summary>
        /// Gets all vehicle models.
        /// </summary>
        /// <returns></returns>
        [HttpGet("models")]
        public Task<ICollection<string>> GetAllVehicleModels() => _service.GetAllModelsAsync();

        /// <summary>
        /// Gets all vehicle manufacturers.
        /// </summary>
        /// <returns></returns>
        [HttpGet("manufacturers")]
        public Task<ICollection<string>> GetAllVehicleManufacturers() => _service.GetAllManufacturersAsync();
    }
}
