﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Pricing.Services.Models;

namespace Pricing.Api.Validation
{
    /// <summary>
    /// Validator for <typeparamref name="TRequest"/>.
    /// </summary>
    /// <typeparam name="TRequest">The type of the request.</typeparam>
    public abstract class SearchRequestValidator<TRequest> : AbstractValidator<TRequest>
        where TRequest : SearchRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchRequestValidator{TRequest}" /> class.
        /// </summary>
        /// <param name="sortFieldWhiteList">The sort field white list.</param>
        protected SearchRequestValidator(IEnumerable<string> sortFieldWhiteList)
        {
            RuleFor(x => x.PageNumber).GreaterThanOrEqualTo(0);
            RuleFor(x => x.PageLength).GreaterThanOrEqualTo(0);
            RuleFor(x => x.SortField).Must(x => sortFieldWhiteList.Contains(x, StringComparer.OrdinalIgnoreCase))
                                     .When(x => !string.IsNullOrEmpty(x.SortField))
                                     .WithMessage("{PropertyName} must be one of " + string.Join(", ", sortFieldWhiteList));
        }
    }
}