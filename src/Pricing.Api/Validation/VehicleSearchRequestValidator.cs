﻿using Pricing.Data;
using Pricing.Services.Models.Vehicle;

namespace Pricing.Api.Validation
{
    /// <summary>
    /// Validator for <see cref="VehicleSearchRequest"/>.
    /// </summary>
    /// <seealso cref="FluentValidation.AbstractValidator{VehicleSearchRequest}" />
    public class VehicleSearchRequestValidator : SearchRequestValidator<VehicleSearchRequest>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateBookSearchRequestValidator" /> class.
        /// </summary>
        public VehicleSearchRequestValidator() : base(VehicleRepository.SortWhiteList)
        {
        }
    }
}