﻿using FluentValidation;
using Pricing.Data;
using Pricing.Services.Models.RateBook;

namespace Pricing.Api.Validation
{
    /// <summary>
    /// Validator for <see cref="RateBookSearchRequest"/>.
    /// </summary>
    /// <seealso cref="FluentValidation.AbstractValidator{RateBookSearchRequest}" />
    public class RateBookSearchRequestValidator : SearchRequestValidator<RateBookSearchRequest>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateBookSearchRequestValidator" /> class.
        /// </summary>
        public RateBookSearchRequestValidator() : base(RateBookRepository.SortWhiteList)
        {
            RuleFor(x => x.Provider).IsInEnum().When(x => x.Provider.HasValue);
        }
    }
}
