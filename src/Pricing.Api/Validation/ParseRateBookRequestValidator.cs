﻿using FluentValidation;
using Humanizer;
using Microsoft.AspNetCore.Http;
using Pricing.Services.Models.RateBook;

namespace Pricing.Api.Validation
{
    /// <summary>
    /// Validator for <see cref="EnqueueParseRateBookRequest"/>.
    /// </summary>
    /// <seealso cref="AbstractValidator{EnqueueParseRateBookRequest}" />
    public class ParseRateBookRequestValidator : AbstractValidator<EnqueueParseRateBookRequest>
    {
        private const long MaximumFileSizeBytes = 20_000_000; // 20MB

        /// <summary>
        /// Initializes a new instance of the <see cref="ParseRateBookRequestValidator" /> class.
        /// </summary>
        public ParseRateBookRequestValidator()
        {
            RuleFor(x => x.Provider).IsInEnum();
            RuleFor(x => x.File).NotNull().WithMessage("Must provide a pricing book file").SetValidator(new FormFileValidator());
        }

        private class FormFileValidator : AbstractValidator<IFormFile>
        {
            public FormFileValidator()
            {
                RuleFor(x => x.FileName).NotEmpty().MaximumLength(255);
                RuleFor(x => x.Length).LessThan(MaximumFileSizeBytes)
                                      .WithMessage("The file must not be larger than " + MaximumFileSizeBytes.Megabytes());
            }
        }
    }
}
