﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Pricing.Api.Extensions
{
    internal static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterAllThatInherit(this IServiceCollection services, Type baseType, Assembly assembly)
        {
            var types = assembly
                       .GetTypes()
                       .Where(x => x.IsClass && !x.IsAbstract && x.HasBaseType(baseType))
                       .SelectMany(x => x.GetInterfaces().Select(i => (service: i, implementation: x)));

            foreach (var (service, implementation) in types)
            {
                services.TryAddTransient(service, implementation);
            }

            return services;
        }

        private static bool HasBaseType(this Type type, Type baseType)
        {
            while (type.BaseType != typeof(object))
            {
                if (type.BaseType == baseType)
                {
                    return true;
                }

                if (type.BaseType.IsGenericType && type.BaseType.IsGenericType && type.BaseType.GetGenericTypeDefinition() == baseType)
                {
                    return true;
                }

                type = type.BaseType;
            }

            return false;
        }
    }
}
