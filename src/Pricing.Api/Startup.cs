﻿using System;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Pricing.Api.Extensions;
using Pricing.Api.Infrastructure;
using Pricing.Data;
using Pricing.Data.Contexts;
using Pricing.Data.Contracts;
using Pricing.Data.Infrastructure;
using Pricing.Services;
using Pricing.Services.Contracts;
using Pricing.Services.Crud;
using Pricing.Services.Parsing;
using Pricing.Services.Util;
using Swashbuckle.AspNetCore.Swagger;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Pricing.Api
{
    public class Startup
    {
        private const string ApplicationName = "Simply Vehicle Leasing Pricing API";
        private readonly IConfiguration _configuration;
        private readonly string _mySqlConnectionString;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
            _mySqlConnectionString = configuration.GetConnectionString("mysql");
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var apiAssembly = typeof(Startup).Assembly;
            var servicesAssembly = typeof(RateBookService).Assembly;
            var dataAssembly = typeof(RateBookRepository).Assembly;

            services.AddMvc(o => o.Filters.Add<ApiExceptionFilter>())
                    .AddJsonOptions(options =>
                                    {
                                        options.SerializerSettings.Converters.Add(new StringEnumConverter());
                                        options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                                    })
                    .AddFluentValidation(o =>
                                         {
                                             o.RegisterValidatorsFromAssembly(apiAssembly);
                                             o.RegisterValidatorsFromAssembly(servicesAssembly);
                                         });
            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddSwaggerGen(c =>
                                   {
                                       c.SwaggerDoc("v1", new Info {Title = ApplicationName, Version = "v1"});
                                       c.OperationFilter<FormFileOperationFilter>();
                                       c.OperationFilter<ActionNameIdOperationFilter>();
                                       c.CustomSchemaIds(t => t.DtoSafeFriendlyId());
                                       c.IncludeXmlComments(apiAssembly.GetXmlFilePath());
                                       c.IncludeXmlComments(servicesAssembly.GetXmlFilePath());
                                       c.IncludeXmlComments(dataAssembly.GetXmlFilePath());
                                   });

            // Services.
            services.AddAutoMapper(c =>
                                   {
                                       c.AddProfiles(servicesAssembly);
                                       c.CreateMissingTypeMaps = false;
                                   });

            services.AddTransient<ISystemClock, SystemClock>();
            services.AddTransient<IRateBookParsingService, RateBookParsingService>();

            services.AddSingleton<RateBookParsingBackgroundQueueService>();
            services.AddTransient<IRateBookParsingBackgroundQueueService>(p => p.GetRequiredService<RateBookParsingBackgroundQueueService>());
            services.AddTransient<IHostedService>(p => p.GetRequiredService<RateBookParsingBackgroundQueueService>());

            services.RegisterAllThatInherit(typeof(EntityFrameworkCrudService<,,,>), servicesAssembly);

            // Parsing.
            services.AddTransient<IProviderRateBookParser, LexAutoleaseRateBookParser>();
            services.AddTransient<IProviderRateBookParser, HitachiRateBookParser>();
            services.AddTransient<IProviderRateBookParser, OgilvieRateBookParser>();

            // Data.
            services.AddDbContext<PricingContext>(o => o.UseMySql(_mySqlConnectionString, c => c.ServerVersion(new Version(5, 7, 21), ServerType.MySql)));
            services.RegisterAllThatInherit(typeof(EntityFrameworkRepository<>), dataAssembly);
            services.AddTransient<IVehicleVariantRepository, VehicleVariantRepository>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(new ExceptionHandlerOptions
                                        {
                                            ExceptionHandler = async context =>
                                                               {
                                                                   if (context.Response.HasStarted)
                                                                   {
                                                                       return;
                                                                   }

                                                                   context.Response.ContentType = "text/plain";
                                                                   await context.Response.WriteAsync("An error has occurred");
                                                               }
                                        });
            }

            if (env.IsDevelopment())
            {
                app.UseCors(o => o.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
                             {
                                 c.DisplayOperationId();
                                 c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{ApplicationName} V1");
                             });
            app.UseMvc();
        }
    }
}
