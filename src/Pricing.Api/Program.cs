﻿using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;

namespace Pricing.Api
{
    public class Program
    {
        public static Task Main(string[] args) => BuildWebHost(args).RunAsync();

        /// <summary>
        /// Builds the web host.
        /// Do not remove. This is used as a design-time hook by EF Core 2.0.
        /// https://docs.microsoft.com/en-us/ef/core/miscellaneous/1x-2x-upgrade#new-way-of-getting-application-services
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                   .UseStartup<Startup>()
                   .UseSerilog((context, configuration) => configuration.ReadFrom.Configuration(context.Configuration))
                   .Build();
    }
}
