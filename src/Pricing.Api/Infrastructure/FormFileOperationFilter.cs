﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pricing.Api.Infrastructure
{
    /// <summary>
    /// Swashbuckle operation that modifies <see cref="IFormFile"/> to be file uploads in the UI.
    /// </summary>
    /// <seealso cref="Swashbuckle.AspNetCore.SwaggerGen.IOperationFilter" />
    public class FormFileOperationFilter : IOperationFilter
    {
        /// <summary>
        /// Applies the specified operation.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <param name="context">The context.</param>
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var description = context.ApiDescription.ParameterDescriptions.FirstOrDefault(x => x.Type == typeof(IFormFile));
            if (description == null)
            {
                return;
            }

            var parameter = operation.Parameters.First(p => p.Name == description.Name);
            operation.Parameters.Remove(parameter);
            operation.Parameters.Add(new NonBodyParameter
                                     {
                                         Name = description.Name,
                                         In = "formData",
                                         Description = "Upload rate book",
                                         Required = true,
                                         Type = "file"
                                     });

            operation.Consumes.Clear();
            operation.Consumes.Add("multipart/form-data");

            // Remove the form file schema if it exists
            context.SchemaRegistry.Definitions.Remove(nameof(IFormFile));
        }
    }
}
