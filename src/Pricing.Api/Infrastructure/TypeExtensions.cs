﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pricing.Api.Infrastructure
{
    public static class TypeExtensions
    {
        public static string DtoSafeFriendlyId(this Type type)
        {
            const string dto = "Dto";
            bool TryClean(string name, out string clean)
            {
                var index = name.IndexOf(dto, StringComparison.Ordinal);
                if (index > 0)
                {
                    clean = name.Substring(0, index);
                    return true;
                }

                clean = null;
                return false;
            }

            if (!type.IsGenericType && TryClean(type.Name, out var cleanName))
            {
                return cleanName;
            }

            if (type.IsGenericType && type.Name.Contains(dto))
            {
                var parameters = type.GenericTypeArguments.Select(t => TryClean(t.Name, out var pt) ? pt : t.Name);
                var name = TryClean(type.Name, out cleanName) ? cleanName : type.Name;
                return $"{name}[{string.Join(",", parameters)}]";
            }


            return type.FriendlyId();
        }

        public static string GetXmlFilePath(this Assembly assembly) => Path.ChangeExtension(assembly.Location, ".xml");
    }
}
