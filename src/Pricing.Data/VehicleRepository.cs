﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pricing.Data.Contexts;
using Pricing.Data.Contracts;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Vehicles;
using Pricing.Data.Infrastructure;

namespace Pricing.Data
{
    /// <summary>
    /// Entity framework repository of <see cref="Vehicle"/>.
    /// </summary>
    public class VehicleRepository : SearchableEntityFrameworkRepository<Vehicle, GetPaginatedVehicleCollectionRequest>, IVehicleRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public VehicleRepository(PricingContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets the sort white list.
        /// </summary>
        public static ICollection<string> SortWhiteList => Expressions.SortWhiteList;

        /// <summary>
        /// Gets all vehicle identifiers.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<ICollection<int>> GetAllIdsAsync(CancellationToken cancellationToken) =>
            await DbSet.Select(x => x.Id).ToListAsync(cancellationToken);

        /// <summary>
        /// Gets all distinct manufacturers in the repository.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<ICollection<string>> GetAllManufacturersAsync(CancellationToken cancellationToken) =>
            await DbSet.Select(x => x.Manufacturer).Distinct().ToListAsync(cancellationToken);

        /// <summary>
        /// Gets all distinct model names in the repository.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<ICollection<string>> GetAllModelsAsync(CancellationToken cancellationToken) =>
            await DbSet.Select(x => x.Model).Distinct().ToListAsync(cancellationToken);

        /// <summary>
        /// Gets a dictionary containing mappings from manufacturers to model names.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<IDictionary<string, ICollection<string>>> GetManufacturerModelDictionaryAsync(CancellationToken cancellationToken) =>
            await DbSet.Select(x => new {x.Model, x.Manufacturer})
                       .GroupBy(x => x.Manufacturer)
                       .ToDictionaryAsync(grp => grp.Key, grp => grp.Select(x => x.Model).ToList() as ICollection<string>, cancellationToken);

        /// <summary>
        /// Gets the vehicle with the specified identifier, including all variants.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public Task<Vehicle> GetWithVariantsAsync(int id, CancellationToken cancellationToken) =>
            DbSet.Include(x => x.Variants).FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
    }
}
