﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pricing.Data.Contexts;
using Pricing.Data.Contracts;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities;
using Pricing.Data.Extensions;
using Pricing.Data.Models;

namespace Pricing.Data.Infrastructure
{
    /// <summary>
    /// A generic Entity Framework Core repository of <typeparamref name="TEntity"/> with searchable support.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TRequest">The type of the request.</typeparam>
    public abstract class SearchableEntityFrameworkRepository<TEntity, TRequest> : EntityFrameworkRepository<TEntity>, ISearchableEntityFrameworkRepository<TEntity, TRequest>
        where TEntity : class, IEntity
        where TRequest : GetPaginatedCollectionRequest
    {
        protected static readonly ExpressionHelper Expressions = new ExpressionHelper();

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchableEntityFrameworkRepository{TEntity,TRequest}" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        protected SearchableEntityFrameworkRepository(PricingContext context) : base(context)
        {
        }

        /// <summary>
        /// Searches for <typeparamref name="TEntity"/> according to the specified <typeparamref name="TRequest"/>.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<PaginatedCollection<TEntity>> SearchAsync(TRequest request, CancellationToken cancellationToken)
        {
            var query = Expressions.GetSearchFilters(request).Aggregate(DbSet.AsQueryable(), (q, f) => q.Where(f));

            if (!string.IsNullOrEmpty(request.SortField))
            {
                if (!Expressions.SortWhiteList.Contains(request.SortField, StringComparer.OrdinalIgnoreCase))
                {
                    throw new ArgumentException($"{request.SortField} is not a whitelisted sort field", nameof(request.SortField));
                }

                query = request.SortDescending ? query.OrderByDescending(request.SortField) : query.OrderBy(request.SortField);
            }

            var result = await query.Skip((request.PageNumber - 1) * request.PageLength).Take(request.PageLength).ToListAsync(cancellationToken);
            var count = await query.CountAsync(cancellationToken);

            return new PaginatedCollection<TEntity>
                   {
                       PageNumber = request.PageNumber,
                       PageLength = request.PageLength,
                       TotalCount = count,
                       Models = result
                   };
        }

        protected class ExpressionHelper
        {
            private readonly ICollection<(PropertyInfo property, SearchOperation operation, Func<TRequest, object> getter)> _searchFields;
            private readonly MethodInfo _stringContainsMethod;

            public ExpressionHelper()
            {
                Func<TRequest, object> GetGetterFunc(PropertyInfo property)
                {
                    var request = Expression.Parameter(typeof(TRequest));
                    var getter = Expression.Property(request, property);
                    var converted = Expression.Convert(getter, typeof(object));
                    var lambda = Expression.Lambda<Func<TRequest, object>>(converted, request);
                    return lambda.Compile();
                }

                _searchFields = typeof(TRequest).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                                .Select(p => (property: p, field: p.GetCustomAttribute<SearchFieldAttribute>(), getter: GetGetterFunc(p)))
                                                .Where(x => x.field != null)
                                                .Select(x =>
                                                        {
                                                            var name = string.IsNullOrEmpty(x.field.FieldName) ? x.property.Name : x.field.FieldName;
                                                            var property = typeof(TEntity).GetProperty(name);
                                                            return (property, x.field.Operation, x.getter);
                                                        })
                                                .ToList();
                SortWhiteList = _searchFields.Select(x => x.property.Name).Distinct().ToList();
                _stringContainsMethod = typeof(string).GetMethod(nameof(string.Contains), new[] { typeof(string) })
                                     ?? throw new ArgumentException("Cannot find string.Contains method");
            }

            public ICollection<string> SortWhiteList { get; }

            public IEnumerable<Expression<Func<TEntity, bool>>> GetSearchFilters(TRequest request)
            {
                var entity = Expression.Parameter(typeof(TEntity), "entity");
                foreach (var (property, operation, getter) in _searchFields)
                {
                    var value = getter(request);
                    if (value == null)
                    {
                        continue;
                    }

                    var body = GetFieldFilter(Expression.Property(entity, property), Expression.Constant(value), operation);
                    yield return Expression.Lambda<Func<TEntity, bool>>(body, entity);
                }
            }

            private Expression GetFieldFilter(Expression member, Expression value, SearchOperation operation)
            {
                switch (operation)
                {
                    case SearchOperation.Eq: return Expression.Equal(member, value);
                    case SearchOperation.Gt: return Expression.GreaterThan(member, value);
                    case SearchOperation.Gte: return Expression.GreaterThanOrEqual(member, value);
                    case SearchOperation.Lt: return Expression.LessThan(member, value);
                    case SearchOperation.Lte: return Expression.LessThanOrEqual(member, value);
                    case SearchOperation.Contains: return Expression.Call(member, _stringContainsMethod, value);
                    default:
                        throw new ArgumentOutOfRangeException(nameof(operation), operation, "Unknown operation");
                }
            }
        }

    }
}