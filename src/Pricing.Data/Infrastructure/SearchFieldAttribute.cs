﻿using System;

namespace Pricing.Data.Infrastructure
{
    public class SearchFieldAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchFieldAttribute"/> class.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        public SearchFieldAttribute(string fieldName = null)
        {
            FieldName = fieldName;
        }

        /// <summary>
        /// Gets the name of the field to search by.
        /// </summary>
        public string FieldName { get; }

        /// <summary>
        /// Gets the operation.
        /// </summary>
        public SearchOperation Operation { get; set; } = SearchOperation.Eq;
    }
}
