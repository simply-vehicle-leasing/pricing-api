﻿namespace Pricing.Data.Infrastructure
{
    public enum SearchOperation
    {
        /// <summary>
        /// An equality operation.
        /// </summary>
        Eq,

        /// <summary>
        /// A greater than operation.
        /// </summary>
        Gt,

        /// <summary>
        /// A greater than or equals operation.
        /// </summary>
        Gte,

        /// <summary>
        /// A less than operation.
        /// </summary>
        Lt,

        /// <summary>
        /// A less than or equals operation.
        /// </summary>
        Lte,

        /// <summary>
        /// A contains operation.
        /// </summary>
        Contains
    }
}