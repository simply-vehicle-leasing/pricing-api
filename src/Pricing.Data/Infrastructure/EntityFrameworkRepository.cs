﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pricing.Data.Contexts;
using Pricing.Data.Contracts;
using Pricing.Data.Entities;

namespace Pricing.Data.Infrastructure
{
    /// <summary>
    /// A generic Entity Framework Core repository of <typeparamref name="TEntity"/>.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public abstract class EntityFrameworkRepository<TEntity> : IEntityFrameworkRepository<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityFrameworkRepository{TEntity}"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        protected EntityFrameworkRepository(PricingContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Gets the EF context.
        /// </summary>
        protected PricingContext Context { get; }

        /// <summary>
        /// Gets the database set for this entity.
        /// </summary>
        protected DbSet<TEntity> DbSet => Context.Set<TEntity>();

        /// <summary>
        /// Determines whether this repository is healthy.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<bool> IsHealthyAsync(CancellationToken cancellationToken = default)
        {
            await DbSet.FirstOrDefaultAsync(cancellationToken);
            return true;
        }

        /// <summary>
        /// Determines whether an <see cref="!:TEntity" /> with the specified identifier exists in this repository.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public Task<bool> ExistsAsync(int id, CancellationToken cancellationToken = default) =>
            DbSet.AnyAsync(x => x.Id == id, cancellationToken);

        /// <summary>
        /// Gets the <typeparamref name="TEntity"/> in this repository with the specified identifier.
        /// Or <c>null</c> if none exist.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public virtual Task<TEntity> GetAsync(int id, CancellationToken cancellationToken = default) =>
                    DbSet.FindAsync(new object[] { id }, cancellationToken);

        /// <summary>
        /// Gets all <typeparamref name="TEntity"/> in this repository.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public virtual async Task<ICollection<TEntity>> GetAllAsync(CancellationToken cancellationToken = default) =>
                    await DbSet.ToListAsync(cancellationToken);

        /// <summary>
        /// Adds the specified <typeparamref name="TEntity"/> to this repository.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public virtual async Task<TEntity> AddAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            await DbSet.AddAsync(entity, cancellationToken);
            return await Context.SaveChangesAsync(cancellationToken) > 0 ? entity : null;
        }

        /// <summary>
        /// Adds the specified <typeparamref name="TEntity"/>'s to this repository.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public virtual async Task<ICollection<TEntity>> AddRangeAsync(ICollection<TEntity> entities, CancellationToken cancellationToken = default)
        {
            await DbSet.AddRangeAsync(entities, cancellationToken);
            return await Context.SaveChangesAsync(cancellationToken) > 0 ? entities : null;
        }

        /// <summary>
        /// Deletes the specified <typeparamref name="TEntity"/>.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            DbSet.Remove(entity);
            return await Context.SaveChangesAsync(cancellationToken) > 0;
        }

        /// <summary>
        /// Deletes the specified <typeparamref name="TEntity" />'s.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<bool> DeleteRangeAsync(ICollection<TEntity> entities, CancellationToken cancellationToken = default)
        {
            DbSet.RemoveRange(entities);
            return await Context.SaveChangesAsync(cancellationToken) > 0;
        }

        /// <summary>
        /// Updates the specified <typeparamref name="TEntity"/>.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public virtual async Task<bool> UpdateAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            if (IsDetached(entity))
            {
                Context.Update(entity);
            }

            return await Context.SaveChangesAsync(cancellationToken) > 0;
        }

        /// <summary>
        /// Determines whether the specified entity is new.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        ///   <c>true</c> if the specified entity is new; otherwise, <c>false</c>.
        /// </returns>
        protected bool IsDetached(TEntity entity) => Context.Entry(entity).State == EntityState.Detached;


    }
}
