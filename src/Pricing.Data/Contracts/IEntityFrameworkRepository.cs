﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Pricing.Data.Entities;

namespace Pricing.Data.Contracts
{
    /// <summary>
    /// A generic Entity Framework Core repository of <typeparamref name="TEntity"/>.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IEntityFrameworkRepository<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// Determines whether this repository is healthy.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<bool> IsHealthyAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Determines whether an <typeparamref name="TEntity"/> with the specified identifier exists in this repository.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<bool> ExistsAsync(int id, CancellationToken cancellationToken = default);

        /// <summary>
        /// Gets the <typeparamref name="TEntity"/> in this repository with the specified identifier.
        /// Or <c>null</c> if none exist.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<TEntity> GetAsync(int id, CancellationToken cancellationToken = default);

        /// <summary>
        /// Gets all <typeparamref name="TEntity"/> in this repository.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<ICollection<TEntity>> GetAllAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Adds the specified <typeparamref name="TEntity"/> to this repository.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<TEntity> AddAsync(TEntity entity, CancellationToken cancellationToken = default);

        /// <summary>
        /// Adds the specified <typeparamref name="TEntity"/>'s to this repository.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<ICollection<TEntity>> AddRangeAsync(ICollection<TEntity> entities, CancellationToken cancellationToken = default);

        /// <summary>
        /// Deletes the specified <typeparamref name="TEntity"/>.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(TEntity entity, CancellationToken cancellationToken = default);

        /// <summary>
        /// Deletes the specified <typeparamref name="TEntity"/>'s.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<bool> DeleteRangeAsync(ICollection<TEntity> entities, CancellationToken cancellationToken = default);

        /// <summary>
        /// Updates the specified <typeparamref name="TEntity"/>.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(TEntity entity, CancellationToken cancellationToken = default);
    }
}
