﻿using System.Threading;
using System.Threading.Tasks;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Rates;

namespace Pricing.Data.Contracts
{
    /// <summary>
    /// Repository of <see cref="RateBook"/>.
    /// </summary>
    public interface IRateBookRepository : ISearchableEntityFrameworkRepository<RateBook, GetPaginatedRateBookCollectionRequest>
    {
        /// <summary>
        /// Gets the rate book with the specified identifier, including all invalid rates.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<RateBook> GetWithInvalidRatesAsync(int id, CancellationToken cancellationToken);
    }
}