﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Pricing.Data.Entities.Background;

namespace Pricing.Data.Contracts
{
    /// <summary>
    /// A repository of <see cref="BackgroundQueueWorkItem"/>.
    /// </summary>
    /// <seealso cref="IEntityFrameworkRepository{BackgroundQueueWorkItem}" />
    public interface IBackgroundQueueWorkItemRepository : IEntityFrameworkRepository<BackgroundQueueWorkItem>
    {
        /// <summary>
        /// Gets all background queue work items with the specified type.
        /// </summary>
        /// <param name="type">The name.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<ICollection<BackgroundQueueWorkItem>> GetAllWithTypeAsync(string type, int limit, CancellationToken cancellationToken = default);
    }
}