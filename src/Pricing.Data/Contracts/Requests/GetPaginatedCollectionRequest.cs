﻿namespace Pricing.Data.Contracts.Requests
{
    /// <summary>
    /// A request to get a paginated view of some collection.
    /// </summary>
    public class GetPaginatedCollectionRequest
    {
        /// <summary>
        /// Gets or sets the current (1-based) page number.
        /// 
        /// If 0 or negative then all pages are returned.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Gets or sets the length of the page.
        /// 
        /// If 0 or negative then all pages are returned.
        /// </summary>
        public int PageLength { get; set; }

        /// <summary>
        /// Gets or sets the name of the field to sort by.
        /// If null then default sort ordering is used.
        /// </summary>
        public string SortField { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to sort descending.
        /// </summary>
        public bool SortDescending { get; set; }
    }
}
