﻿using System;
using Pricing.Data.Entities.Rates;
using Pricing.Data.Infrastructure;
using Pricing.Data.Models;

namespace Pricing.Data.Contracts.Requests
{
    /// <summary>
    /// A request to get a paginated collection of <see cref="RateBook"/>.
    /// </summary>
    /// <seealso cref="GetPaginatedCollectionRequest" />
    public class GetPaginatedRateBookCollectionRequest : GetPaginatedCollectionRequest
    {
        /// <summary>
        /// Gets or sets an optional rate book provider filter.
        /// </summary>
        [SearchField]
        public RateBookProvider? Provider { get; set; }

        /// <summary>
        /// Gets or sets an optional filename search query.
        /// <c>null</c> means match all files.
        /// </summary>
        [SearchField]
        public string Filename { get; set; }

        /// <summary>
        /// Gets or sets an optional minimum date added filter.
        /// </summary>
        [SearchField(nameof(RateBook.DateAdded), Operation = SearchOperation.Gte)]
        public DateTime? MinimumDateAdded { get; set; }

        /// <summary>
        /// Gets or sets an optional maximum date added filter.
        /// </summary>
        [SearchField(nameof(RateBook.DateAdded), Operation = SearchOperation.Lte)]
        public DateTime? MaximumDateAdded { get; set; }
    }
}
