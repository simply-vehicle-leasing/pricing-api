﻿using Pricing.Data.Entities;
using Pricing.Data.Entities.Vehicles;
using Pricing.Data.Infrastructure;
using Pricing.Data.Models;

namespace Pricing.Data.Contracts.Requests
{
    /// <summary>
    /// A request to get a paginated collection of <see cref="Vehicle"/>.
    /// </summary>
    /// <seealso cref="GetPaginatedCollectionRequest" />
    public class GetPaginatedVehicleCollectionRequest : GetPaginatedCollectionRequest
    {
        /// <summary>
        /// Gets or sets an optional vehicle manufacturer filter.
        /// </summary>
        [SearchField]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets an optional vehicle model filter.
        /// </summary>
        [SearchField]
        public string Model { get; set; }
    }
}
