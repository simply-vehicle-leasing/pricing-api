﻿using System.Threading;
using System.Threading.Tasks;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities;
using Pricing.Data.Models;

namespace Pricing.Data.Contracts
{
    /// <summary>
    /// A generic Entity Framework Core repository of <typeparamref name="TEntity"/> with searchable support.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TRequest">The type of the request.</typeparam>
    public interface ISearchableEntityFrameworkRepository<TEntity, in TRequest> : IEntityFrameworkRepository<TEntity>
        where TEntity : class, IEntity
        where TRequest : GetPaginatedCollectionRequest
    {
        /// <summary>
        /// Searches for <typeparamref name="TEntity"/> according to the specified <typeparamref name="TRequest"/>.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<PaginatedCollection<TEntity>> SearchAsync(TRequest request, CancellationToken cancellationToken);
    }
}