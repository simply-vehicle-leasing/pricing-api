﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Vehicles;

namespace Pricing.Data.Contracts
{
    /// <summary>
    /// A repository of <see cref="Vehicle"/>.
    /// </summary>
    public interface IVehicleRepository : ISearchableEntityFrameworkRepository<Vehicle, GetPaginatedVehicleCollectionRequest>
    {
        /// <summary>
        /// Gets all vehicle identifiers.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<ICollection<int>> GetAllIdsAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Gets all distinct manufacturers in the repository.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<ICollection<string>> GetAllManufacturersAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Gets all distinct model names in the repository.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<ICollection<string>> GetAllModelsAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Gets a dictionary containing mappings from manufacturers to model names.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<IDictionary<string, ICollection<string>>> GetManufacturerModelDictionaryAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Gets the vehicle with the specified identifier, including all variants.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<Vehicle> GetWithVariantsAsync(int id, CancellationToken cancellationToken);
    }
}