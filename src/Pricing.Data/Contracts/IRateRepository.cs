﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Rates;
using Pricing.Data.Models;

namespace Pricing.Data.Contracts
{
    /// <summary>
    /// Repository of <see cref="Rate"/>.
    /// </summary>/>
    public interface IRateRepository : IEntityFrameworkRepository<Rate>
    {
        /// <summary>
        /// Gets all rates with the specified CAP code.
        /// </summary>
        /// <param name="capCode">The cap code.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<ICollection<Rate>> GetByCapCodeAsync(string capCode, CancellationToken cancellationToken);

        /// <summary>
        /// Gets all rates with the specified manufacturer/model hash.
        /// </summary>
        /// <param name="vehicleId">The manufacturer/model hash.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<ICollection<Rate>> GetByVehicleAsync(int vehicleId, CancellationToken cancellationToken);

        /// <summary>
        /// Deletes all rates with the specified provider and profiles.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="profiles">The profiles.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>The total number of rates deleted</returns>
        Task<int> DeleteByProfilesAsync(RateBookProvider provider, ICollection<ContractProfile> profiles, CancellationToken cancellationToken);
    }
}