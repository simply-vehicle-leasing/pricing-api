﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Vehicles;

namespace Pricing.Data.Contracts
{
    /// <summary>
    /// Repository of vehicle variants.
    /// </summary>
    public interface IVehicleVariantRepository
    {
        /// <summary>
        /// Gets all cap codes.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task<ICollection<string>> GetAllCapCodesAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Adds the specified vehicle variants to this repository.
        /// </summary>
        /// <param name="models">The models.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        Task AddRangeAsync(ICollection<VehicleVariant> models, CancellationToken cancellationToken);
    }
}