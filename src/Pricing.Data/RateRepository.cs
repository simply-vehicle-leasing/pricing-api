﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pricing.Data.Contexts;
using Pricing.Data.Contracts;
using Pricing.Data.Entities.Rates;
using Pricing.Data.Infrastructure;
using Pricing.Data.Models;

namespace Pricing.Data
{
    /// <summary>
    /// Entity framework repository of <see cref="Rate" />.
    /// </summary>
    /// <seealso cref="Pricing.Data.Infrastructure.EntityFrameworkRepository{Rate}" />
    /// <seealso cref="Pricing.Data.Contracts.IRateRepository" />
    public class RateRepository : EntityFrameworkRepository<Rate>, IRateRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public RateRepository(PricingContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets all rates with the specified CAP code.
        /// </summary>
        /// <param name="capCode">The cap code.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<ICollection<Rate>> GetByCapCodeAsync(string capCode, CancellationToken cancellationToken) =>
            await DbSet.Where(x => x.CapCode == capCode).ToListAsync(cancellationToken);

        /// <summary>
        /// Gets all rates with the specified vehicle.
        /// </summary>
        /// <param name="vehicleId">The vehicle identifier.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<ICollection<Rate>> GetByVehicleAsync(int vehicleId, CancellationToken cancellationToken) =>
            await DbSet.Where(x => x.VehicleId == vehicleId).ToListAsync(cancellationToken);

        /// <summary>
        /// Deletes the by profiles asynchronous.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="profiles">The profiles.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>The total number of rates deleted</returns>
        public async Task<int> DeleteByProfilesAsync(RateBookProvider provider, ICollection<ContractProfile> profiles, CancellationToken cancellationToken)
        {
            // TODO: avoid trip to DB.

            if (profiles.Count == 0)
            {
                return 0;
            }

            var rate = Expression.Parameter(typeof(Rate), "rate");
            var providerProperty = Expression.Property(rate, nameof(Rate.Provider));
            var termProperty = Expression.Property(rate, nameof(Rate.Term));
            var mileageProperty = Expression.Property(rate, nameof(Rate.Mileage));

            Expression ProfileExpression(ContractProfile profile) => Expression.And(Expression.Equal(Expression.Constant(profile.Term), termProperty),
                                                                                    Expression.Equal(Expression.Constant(profile.Mileage), mileageProperty));

            Expression profileExpression = null;
            foreach (var profile in profiles.Select(ProfileExpression))
            {
                if (profileExpression == null)
                {
                    profileExpression = profile;
                }

                profileExpression = Expression.Or(profileExpression, profile);
            }

            var expression = Expression.And(Expression.Equal(Expression.Constant(provider), providerProperty), profileExpression);
            var lambda = Expression.Lambda<Func<Rate, bool>>(expression, rate);

            var entities = await DbSet.Where(lambda).ToListAsync(cancellationToken);
            if (!entities.Any())
            {
                return 0;
            }

            DbSet.RemoveRange(entities);
            return await Context.SaveChangesAsync(cancellationToken);

        }
    }
}
