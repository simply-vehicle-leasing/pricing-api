﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pricing.Data.Contexts;
using Pricing.Data.Contracts;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Vehicles;
using Pricing.Data.Infrastructure;

namespace Pricing.Data
{
    /// <summary>
    /// Entity framework repository of <see cref="VehicleVariant"/>.
    /// </summary>
    public class VehicleVariantRepository : IVehicleVariantRepository
    {
        private readonly PricingContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleVariantRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public VehicleVariantRepository(PricingContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets all cap codes.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<ICollection<string>> GetAllCapCodesAsync(CancellationToken cancellationToken) =>
            await _context.VehicleVariants.Select(x => x.CapCode).ToListAsync(cancellationToken);

        /// <summary>
        /// Adds the specified vehicle variants to this repository.
        /// </summary>
        /// <param name="models">The models.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task AddRangeAsync(ICollection<VehicleVariant> models, CancellationToken cancellationToken)
        {
            await _context.VehicleVariants.AddRangeAsync(models, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
        }
            
    }
}
