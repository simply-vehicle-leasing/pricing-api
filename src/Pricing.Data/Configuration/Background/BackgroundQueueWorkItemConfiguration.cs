﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pricing.Data.Entities.Background;
using Pricing.Data.Extensions;

namespace Pricing.Data.Configuration.Background
{
    /// <summary>
    /// Configuration for <see cref="BackgroundQueueWorkItem"/>.
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{BackgroundQueueWorkItem}" />
    public class BackgroundQueueWorkItemConfiguration : IEntityTypeConfiguration<BackgroundQueueWorkItem>
    {
        /// <summary>
        /// Configures the entity of type <see cref="BackgroundQueueWorkItem"/>.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<BackgroundQueueWorkItem> builder)
        {
            builder.HasIndex(x => x.Queued);
            builder.HasIndex(x => x.Type);

            builder.ToTable("BackgroundQueueWorkItems");
            builder.Property(x => x.Name).ShortVarchar().IsRequired();
            builder.Property(x => x.Type).RequiredIndexedVarchar();
            builder.Property(x => x.Error).LongVarchar();
        }
    }
}
