﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Vehicles;
using Pricing.Data.Extensions;

namespace Pricing.Data.Configuration.Vehicles
{
    /// <summary>
    /// Configuration for <see cref="Vehicle"/>.
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{Vehicle}" />
    public class VehicleConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        /// <summary>
        /// Configures the entity of type <see cref="Vehicle"/>.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.Property(x => x.Id).ValueGeneratedNever();

            builder.HasIndex(x => x.Manufacturer);
            builder.HasIndex(x => x.Model);

            builder.Property(x => x.Manufacturer).RequiredIndexedVarchar();
            builder.Property(x => x.Model).RequiredIndexedVarchar();

            builder.HasMany(x => x.Rates).WithOne().HasForeignKey(x => x.VehicleId).IsRequired();
            builder.HasMany(x => x.Variants).WithOne().HasForeignKey(x => x.VehicleId).IsRequired();
        }
    }
}
