﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pricing.Data.Entities.Vehicles;
using Pricing.Data.Extensions;

namespace Pricing.Data.Configuration.Vehicles
{
    /// <summary>
    /// Configuration for <see cref="VehicleVariant"/>.
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{VehicleVariant}" />
    public class VehicleVariantConfiguration : IEntityTypeConfiguration<VehicleVariant>
    {
        /// <summary>
        /// Configures the entity of type <see cref="VehicleVariant" />.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<VehicleVariant> builder)
        {
            builder.HasKey(x => x.CapCode);

            builder.Property(x => x.CapCode).RequiredIndexedVarchar();
            builder.Property(x => x.Name).RequiredIndexedVarchar();
            builder.Property(x => x.BodyType).RequiredIndexedVarchar();
            builder.Property(x => x.P11D);

            builder.HasMany(x => x.Rates).WithOne(x => x.VehicleVariant).HasForeignKey(x => x.CapCode).IsRequired();
        }
    }
}