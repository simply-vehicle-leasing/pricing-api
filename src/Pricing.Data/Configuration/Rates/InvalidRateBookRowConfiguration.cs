﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pricing.Data.Entities.Rates;
using Pricing.Data.Extensions;

namespace Pricing.Data.Configuration.Rates
{
    /// <summary>
    /// Configuration for <see cref="IEntityTypeConfiguration{TEntity}"/>.
    /// </summary>
    /// <seealso cref="InvalidRateBookRow" />
    public class InvalidRateBookRowConfiguration : IEntityTypeConfiguration<InvalidRateBookRow>
    {
        /// <summary>
        /// Configures the entity of type <see cref="InvalidRateBookRow"/>.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<InvalidRateBookRow> builder)
        {
            builder.ToTable("InvalidRateBookRows");
            builder.Property(x => x.CapCode).RequiredIndexedVarchar();
            builder.Property(x => x.ValidationErrors).LongVarchar().IsRequired();
        }
    }
}