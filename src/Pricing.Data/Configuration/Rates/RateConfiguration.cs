﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pricing.Data.Entities.Rates;
using Pricing.Data.Extensions;

namespace Pricing.Data.Configuration.Rates
{
    /// <summary>
    /// Configuration for <see cref="Rate"/>.
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{Rate}" />
    public class RateConfiguration : IEntityTypeConfiguration<Rate>
    {
        /// <summary>
        /// Configures the entity of type <see cref="Rate"/>.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<Rate> builder)
        {
            builder.HasIndex(x => x.ContractFeatures);
            builder.HasIndex(x => x.ContractSubject);
            builder.HasIndex(x => x.Term);
            builder.HasIndex(x => x.Mileage);
            builder.HasIndex(x => x.Provider);

            builder.Property(x => x.CapCode).RequiredIndexedVarchar();
            builder.Property(x => x.InitialPayment);
            builder.Property(x => x.MonthlyPayment);
            builder.Property(x => x.TotalCost);
            builder.Property(x => x.ExcessMileageCost);
        }
    }
}