﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Rates;
using Pricing.Data.Extensions;

namespace Pricing.Data.Configuration.Rates
{
    /// <summary>
    /// Configuration for <see cref="RateBook"/>.
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{RateBook}" />
    public class RateBookConfiguration : IEntityTypeConfiguration<RateBook>
    {
        /// <summary>
        /// Configures the entity of type <see cref="RateBook"/>.
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity type.</param>
        public void Configure(EntityTypeBuilder<RateBook> builder)
        {
            builder.HasIndex(x => x.Filename);

            builder.Property(x => x.Filename).RequiredIndexedVarchar();

            builder.HasMany(x => x.InvalidRows).WithOne().HasForeignKey(x => x.RateBookId).IsRequired();
        }
    }
}