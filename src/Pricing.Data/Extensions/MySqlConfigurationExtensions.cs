﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MySql.Data.MySqlClient;

namespace Pricing.Data.Extensions
{
    internal static class MySqlConfigurationExtensions
    {
        public const int LongStringLength = 4096;
        public const int OptimumStringLength = 255;
        public const int IndexedStringLength = 191; // 767 / 4

        public static readonly string Date = MySqlDbType.Date.ToString().ToLower();

        public static PropertyBuilder<TProperty> RequiredIndexedVarchar<TProperty>(this PropertyBuilder<TProperty> propertyBuilder) =>
            propertyBuilder.HasMaxLength(IndexedStringLength).IsRequired();

        public static PropertyBuilder<TProperty> ShortVarchar<TProperty>(this PropertyBuilder<TProperty> propertyBuilder) =>
            propertyBuilder.HasMaxLength(OptimumStringLength);

        public static PropertyBuilder<TProperty> LongVarchar<TProperty>(this PropertyBuilder<TProperty> propertyBuilder) =>
            propertyBuilder.HasMaxLength(LongStringLength);
    }
}
