﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Pricing.Data.Extensions
{
    public static class MagicStringLinqExtensions
    {
        private static readonly ConcurrentDictionary<Type, object> LinqCaches = new ConcurrentDictionary<Type, object>();

        public static IQueryable<TModel> OrderBy<TModel>(this IQueryable<TModel> models, string propertyName) =>
            GetLinqCache<TModel>().OrderBy(models, propertyName);

        public static IQueryable<TModel> OrderByDescending<TModel>(this IQueryable<TModel> models, string propertyName) =>
            GetLinqCache<TModel>().OrderByDescending(models, propertyName);

        private static LinqCache<TModel> GetLinqCache<TModel>() =>
            (LinqCache<TModel>)LinqCaches.GetOrAdd(typeof(TModel), t => new LinqCache<TModel>());

        private class LinqCache<TModel>
        {
            private readonly IDictionary<string, (object getter, MethodInfo orderBy, MethodInfo orderByDescending)> _properties;

            public LinqCache()
            {
                var orderBy = typeof(Queryable).GetMethods().FirstOrDefault(m => m.Name == nameof(Queryable.OrderBy) && m.GetParameters().Length == 2)
                           ?? throw new ArgumentException($"Cannot find {nameof(Queryable.OrderBy)} method");
                var orderByDescending = typeof(Queryable).GetMethods().FirstOrDefault(m => m.Name == nameof(Queryable.OrderByDescending) && m.GetParameters().Length == 2)
                                     ?? throw new ArgumentException($"Cannot find {nameof(Queryable.OrderByDescending)} method");

                _properties = typeof(TModel)
                             .GetProperties()
                             .ToDictionary(x => x.Name,
                                           x => (GetFunc(x),
                                                    orderBy.MakeGenericMethod(typeof(TModel), x.PropertyType),
                                                    orderByDescending.MakeGenericMethod(typeof(TModel), x.PropertyType)),
                                           StringComparer.OrdinalIgnoreCase);
            }

            private object GetFunc(PropertyInfo property)
            {
                var parameter = Expression.Parameter(typeof(TModel));
                return Expression.Lambda(Expression.Property(parameter, property), parameter);
            }

            public IQueryable<TModel> OrderBy(IQueryable<TModel> models, string propertyName)
            {
                var (getter, orderBy, _) = GetMethods(propertyName);
                return (IQueryable<TModel>)orderBy.Invoke(null, new[] { models, getter });
            }

            public IQueryable<TModel> OrderByDescending(IQueryable<TModel> models, string propertyName)
            {
                var (getter, _, orderByDescending) = GetMethods(propertyName);
                return (IQueryable<TModel>)orderByDescending.Invoke(null, new[] { models, getter });
            }

            private (object getter, MethodInfo orderBy, MethodInfo orderByDescending) GetMethods(string property)
            {
                if (!_properties.TryGetValue(property, out var methods))
                {
                    throw new ArgumentException($"No {property} property on {typeof(TModel)}");
                }

                return methods;
            }
        }
    }
}
