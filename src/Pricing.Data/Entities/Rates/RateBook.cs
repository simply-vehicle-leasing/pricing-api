﻿using System;
using System.Collections.Generic;
using Pricing.Data.Models;

namespace Pricing.Data.Entities.Rates
{
    /// <summary>
    /// A pricing book.
    /// </summary>
    public class RateBook : IEntity
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a hash of the original pricing book file.
        /// </summary>
        public int FileHash { get; set; }

        /// <summary>
        /// Gets or sets the provider.
        /// </summary>
        public RateBookProvider Provider { get; set; }

        /// <summary>
        /// Gets or sets the filename of the pricing book.
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Gets or sets the size of the file in bytes.
        /// </summary>
        public int FileSize { get; set; }

        /// <summary>
        /// Gets or sets the date added.
        /// </summary>
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets the rate count.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Gets the invalid rate count.
        /// </summary>
        public int InvalidRowCount { get; set; }

        /// <summary>
        /// Gets or sets the invalid rates.
        /// </summary>
        public virtual ICollection<InvalidRateBookRow> InvalidRows { get; set; }
    }
}
