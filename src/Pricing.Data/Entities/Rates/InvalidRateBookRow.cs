﻿namespace Pricing.Data.Entities.Rates
{
    /// <summary>
    /// An invalid rate parsed from a pricing book.
    /// </summary>
    public class InvalidRateBookRow : IEntity
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the rate book identifier.
        /// </summary>
        public int RateBookId { get; set; }

        /// <summary>
        /// Gets or sets the row number.
        /// </summary>
        public int RowNumber { get; set; }

        /// <summary>
        /// Gets or sets the CAP code.
        /// </summary>
        public string CapCode { get; set; }

        /// <summary>
        /// Gets or sets the validation errors.
        /// </summary>
        public string ValidationErrors { get; set; }
    }
}
