﻿using System;

namespace Pricing.Data.Entities.Background
{
    /// <summary>
    /// A background work item.
    /// </summary>
    public class BackgroundQueueWorkItem : IEntity
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the date time that the work item was queued.
        /// </summary>
        public DateTime Queued { get; set; }

        /// <summary>
        /// Gets or sets the date time that the work item was executed.
        /// </summary>
        public DateTime? Executed { get; set; }

        /// <summary>
        /// Gets or sets the date time that the work item was completed.
        /// </summary>
        public DateTime? Completed { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public BackgroundQueueWorkItemStatus Status => Completed.HasValue ? BackgroundQueueWorkItemStatus.Completed :
                                                       !string.IsNullOrEmpty(Error) ? BackgroundQueueWorkItemStatus.Failed :
                                                       Executed.HasValue ? BackgroundQueueWorkItemStatus.Running :
                                                       BackgroundQueueWorkItemStatus.Queued;

        /// <summary>
        /// Gets the queued time.
        /// </summary>
        public TimeSpan QueuedTime => (Executed ?? DateTime.UtcNow) - Queued;

        /// <summary>
        /// Gets the execution time.
        /// </summary>
        public TimeSpan ExecutionTime => Executed.HasValue ? (Completed ?? DateTime.UtcNow) - Executed.Value : TimeSpan.Zero;
    }
}
