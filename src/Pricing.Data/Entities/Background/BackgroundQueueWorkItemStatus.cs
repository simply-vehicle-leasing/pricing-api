﻿namespace Pricing.Data.Entities.Background
{
    /// <summary>
    /// The status of a background work item.
    /// </summary>
    public enum BackgroundQueueWorkItemStatus
    {
        /// <summary>
        /// The background work item is queued.
        /// </summary>
        Queued = 1,

        /// <summary>
        /// The background work item is running.
        /// </summary>
        Running = 2,

        /// <summary>
        /// The background work item is completed.
        /// </summary>
        Completed = 3,

        /// <summary>
        /// The background work item is failed.
        /// </summary>
        Failed = 4,
    }
}