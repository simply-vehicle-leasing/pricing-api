﻿namespace Pricing.Data.Entities
{
    /// <summary>
    /// An EF entity with an integer key.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        int Id { get; set; }
    }
}
