﻿using System.Collections.Generic;

namespace Pricing.Data.Entities.Vehicles
{
    public class VehicleVariant
    {
        /// <summary>
        /// Gets or sets the cap code.
        /// </summary>
        public string CapCode { get; set; }

        /// <summary>
        /// Gets or sets the vehicle identifier.
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Gets or sets the vehicle variant name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the type of the body.
        /// </summary>
        public string BodyType { get; set; }

        /// <summary>
        /// Gets or sets the P11D price.
        /// </summary>
        public decimal? P11D { get; set; }

        /// <summary>
        /// Gets or sets the rates.
        /// </summary>
        public virtual ICollection<Rates.Rate> Rates { get; set; }
    }
}
