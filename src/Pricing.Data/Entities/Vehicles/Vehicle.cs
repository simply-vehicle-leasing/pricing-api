﻿using System.Collections.Generic;
using Pricing.Data.Entities.Rates;

namespace Pricing.Data.Entities.Vehicles
{
    /// <summary>
    /// A vehicle.
    /// </summary>
    public class Vehicle : IEntity
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer of the vehicle.
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the model of the vehicle.
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Gets or sets the rates.
        /// </summary>
        public virtual ICollection<Rate> Rates { get; set; }

        /// <summary>
        /// Gets or sets the variants of the vehicle.
        /// </summary>
        public virtual ICollection<VehicleVariant> Variants { get; set; }
    }
}
