﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pricing.Data.Contexts;
using Pricing.Data.Contracts;
using Pricing.Data.Entities.Background;
using Pricing.Data.Infrastructure;

namespace Pricing.Data
{
    /// <summary>
    /// A repository of <see cref="BackgroundQueueWorkItem"/>.
    /// </summary>
    /// <seealso cref="EntityFrameworkRepository{BackgroundQueueWorkItem}" />
    /// <seealso cref="IBackgroundQueueWorkItemRepository" />
    public class BackgroundQueueWorkItemRepository : EntityFrameworkRepository<BackgroundQueueWorkItem>, IBackgroundQueueWorkItemRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BackgroundQueueWorkItemRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public BackgroundQueueWorkItemRepository(PricingContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets all background queue work items with the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public async Task<ICollection<BackgroundQueueWorkItem>> GetAllWithTypeAsync(string type, int limit, CancellationToken cancellationToken = default) =>
            await DbSet.Where(x => x.Type == type)
                       .OrderByDescending(x => x.Queued)
                       .Take(limit)
                       .ToListAsync(cancellationToken);

    }
}
