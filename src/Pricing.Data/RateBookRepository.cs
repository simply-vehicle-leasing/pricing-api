﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pricing.Data.Contexts;
using Pricing.Data.Contracts;
using Pricing.Data.Contracts.Requests;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Rates;
using Pricing.Data.Infrastructure;

namespace Pricing.Data
{
    /// <summary>
    /// Entity framework repository of <see cref="RateBook"/>.
    /// </summary>
    public class RateBookRepository : SearchableEntityFrameworkRepository<RateBook, GetPaginatedRateBookCollectionRequest>, IRateBookRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateBookRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public RateBookRepository(PricingContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets the sort white list.
        /// </summary>
        public static ICollection<string> SortWhiteList => Expressions.SortWhiteList;

        /// <summary>
        /// Gets the rate book with the specified identifier, including all invalid rates.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns></returns>
        public Task<RateBook> GetWithInvalidRatesAsync(int id, CancellationToken cancellationToken) =>
            DbSet.Include(x => x.InvalidRows).FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
    }
}
