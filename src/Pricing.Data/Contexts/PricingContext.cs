﻿using Microsoft.EntityFrameworkCore;
using Pricing.Data.Entities;
using Pricing.Data.Entities.Background;
using Pricing.Data.Entities.Rates;
using Pricing.Data.Entities.Vehicles;
using Pricing.Data.Extensions;

namespace Pricing.Data.Contexts
{
    /// <summary>
    /// Pricing context.
    /// </summary>
    /// <seealso cref="DbContext" />
    public class PricingContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PricingContext"/> class.
        /// </summary>
        /// <param name="options">The options.</param>
        public PricingContext(DbContextOptions<PricingContext> options) : base(options)
        {
        }

        /// <summary>
        /// Loads all EF configuration.
        /// </summary>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationFromAssembly(typeof(PricingContext).Assembly);
        }

        /// <summary>
        /// Gets or sets the vehicles.
        /// </summary>
        public DbSet<Vehicle> Vehicles { get; set; }

        /// <summary>
        /// Gets or sets the vehicle variants.
        /// </summary>
        public DbSet<VehicleVariant> VehicleVariants { get; set; }

        /// <summary>
        /// Gets or sets the rates.
        /// </summary>
        public DbSet<Rate> Rates { get; set; }

        /// <summary>
        /// Gets or sets the rate books.
        /// </summary>
        public DbSet<RateBook> RateBooks { get; set; }

        /// <summary>
        /// Gets or sets the background queue work items.
        /// </summary>
        public DbSet<BackgroundQueueWorkItem> BackgroundQueueWorkItems { get; set; }
    }
}
