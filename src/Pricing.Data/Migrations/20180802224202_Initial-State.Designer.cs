﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Pricing.Data.Contexts;

namespace Pricing.Data.Migrations
{
    [DbContext(typeof(PricingContext))]
    [Migration("20180802224202_Initial-State")]
    partial class InitialState
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.1-rtm-30846")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("Pricing.Data.Entities.Background.BackgroundQueueWorkItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("Completed");

                    b.Property<string>("Error")
                        .HasMaxLength(4096);

                    b.Property<DateTime?>("Executed");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<DateTime>("Queued");

                    b.Property<string>("Type")
                        .IsRequired()
                        .HasMaxLength(191);

                    b.HasKey("Id");

                    b.HasIndex("Queued");

                    b.HasIndex("Type");

                    b.ToTable("BackgroundQueueWorkItems");
                });

            modelBuilder.Entity("Pricing.Data.Entities.Rates.InvalidRateBookRow", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CapCode")
                        .IsRequired()
                        .HasMaxLength(191);

                    b.Property<int>("RateBookId");

                    b.Property<int>("RowNumber");

                    b.Property<string>("ValidationErrors")
                        .IsRequired()
                        .HasMaxLength(4096);

                    b.HasKey("Id");

                    b.HasIndex("RateBookId");

                    b.ToTable("InvalidRateBookRows");
                });

            modelBuilder.Entity("Pricing.Data.Entities.Rates.Rate", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CapCode")
                        .IsRequired()
                        .HasMaxLength(191);

                    b.Property<byte>("ContractFeatures");

                    b.Property<byte>("ContractSubject");

                    b.Property<decimal>("ExcessMileageCost");

                    b.Property<decimal>("InitialPayment");

                    b.Property<short>("InitialPayments");

                    b.Property<int>("Mileage");

                    b.Property<decimal>("MonthlyPayment");

                    b.Property<byte>("Provider");

                    b.Property<int?>("RateBookId");

                    b.Property<short>("Term");

                    b.Property<decimal>("TotalCost");

                    b.Property<int?>("ValueScore");

                    b.Property<int>("VehicleId");

                    b.HasKey("Id");

                    b.HasIndex("CapCode");

                    b.HasIndex("ContractFeatures");

                    b.HasIndex("ContractSubject");

                    b.HasIndex("Mileage");

                    b.HasIndex("Provider");

                    b.HasIndex("RateBookId");

                    b.HasIndex("Term");

                    b.HasIndex("VehicleId");

                    b.ToTable("Rates");
                });

            modelBuilder.Entity("Pricing.Data.Entities.Rates.RateBook", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Count");

                    b.Property<DateTime>("DateAdded");

                    b.Property<int>("FileHash");

                    b.Property<int>("FileSize");

                    b.Property<string>("Filename")
                        .IsRequired()
                        .HasMaxLength(191);

                    b.Property<int>("InvalidRowCount");

                    b.Property<byte>("Provider");

                    b.HasKey("Id");

                    b.HasIndex("Filename");

                    b.ToTable("RateBooks");
                });

            modelBuilder.Entity("Pricing.Data.Entities.Vehicles.Vehicle", b =>
                {
                    b.Property<int>("Id");

                    b.Property<string>("Manufacturer")
                        .IsRequired()
                        .HasMaxLength(191);

                    b.Property<string>("Model")
                        .IsRequired()
                        .HasMaxLength(191);

                    b.HasKey("Id");

                    b.HasIndex("Manufacturer");

                    b.HasIndex("Model");

                    b.ToTable("Vehicles");
                });

            modelBuilder.Entity("Pricing.Data.Entities.Vehicles.VehicleVariant", b =>
                {
                    b.Property<string>("CapCode")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(191);

                    b.Property<string>("BodyType")
                        .IsRequired()
                        .HasMaxLength(191);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(191);

                    b.Property<decimal?>("P11D");

                    b.Property<int>("VehicleId");

                    b.HasKey("CapCode");

                    b.HasIndex("VehicleId");

                    b.ToTable("VehicleVariants");
                });

            modelBuilder.Entity("Pricing.Data.Entities.Rates.InvalidRateBookRow", b =>
                {
                    b.HasOne("Pricing.Data.Entities.Rates.RateBook")
                        .WithMany("InvalidRows")
                        .HasForeignKey("RateBookId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Pricing.Data.Entities.Rates.Rate", b =>
                {
                    b.HasOne("Pricing.Data.Entities.Vehicles.VehicleVariant", "VehicleVariant")
                        .WithMany("Rates")
                        .HasForeignKey("CapCode")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Pricing.Data.Entities.Rates.RateBook", "RateBook")
                        .WithMany()
                        .HasForeignKey("RateBookId");

                    b.HasOne("Pricing.Data.Entities.Vehicles.Vehicle")
                        .WithMany("Rates")
                        .HasForeignKey("VehicleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Pricing.Data.Entities.Vehicles.VehicleVariant", b =>
                {
                    b.HasOne("Pricing.Data.Entities.Vehicles.Vehicle")
                        .WithMany("Variants")
                        .HasForeignKey("VehicleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
