﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Pricing.Data.Migrations
{
    public partial class InitialState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BackgroundQueueWorkItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Type = table.Column<string>(maxLength: 191, nullable: false),
                    Queued = table.Column<DateTime>(nullable: false),
                    Executed = table.Column<DateTime>(nullable: true),
                    Completed = table.Column<DateTime>(nullable: true),
                    Error = table.Column<string>(maxLength: 4096, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BackgroundQueueWorkItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RateBooks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FileHash = table.Column<int>(nullable: false),
                    Provider = table.Column<byte>(nullable: false),
                    Filename = table.Column<string>(maxLength: 191, nullable: false),
                    FileSize = table.Column<int>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    Count = table.Column<int>(nullable: false),
                    InvalidRowCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RateBooks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Manufacturer = table.Column<string>(maxLength: 191, nullable: false),
                    Model = table.Column<string>(maxLength: 191, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InvalidRateBookRows",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RateBookId = table.Column<int>(nullable: false),
                    RowNumber = table.Column<int>(nullable: false),
                    CapCode = table.Column<string>(maxLength: 191, nullable: false),
                    ValidationErrors = table.Column<string>(maxLength: 4096, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvalidRateBookRows", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InvalidRateBookRows_RateBooks_RateBookId",
                        column: x => x.RateBookId,
                        principalTable: "RateBooks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleVariants",
                columns: table => new
                {
                    CapCode = table.Column<string>(maxLength: 191, nullable: false),
                    VehicleId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 191, nullable: false),
                    BodyType = table.Column<string>(maxLength: 191, nullable: false),
                    P11D = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleVariants", x => x.CapCode);
                    table.ForeignKey(
                        name: "FK_VehicleVariants_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Rates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    VehicleId = table.Column<int>(nullable: false),
                    CapCode = table.Column<string>(maxLength: 191, nullable: false),
                    Term = table.Column<short>(nullable: false),
                    Mileage = table.Column<int>(nullable: false),
                    InitialPayments = table.Column<short>(nullable: false),
                    InitialPayment = table.Column<decimal>(nullable: false),
                    MonthlyPayment = table.Column<decimal>(nullable: false),
                    TotalCost = table.Column<decimal>(nullable: false),
                    ExcessMileageCost = table.Column<decimal>(nullable: false),
                    ValueScore = table.Column<int>(nullable: true),
                    Provider = table.Column<byte>(nullable: false),
                    ContractSubject = table.Column<byte>(nullable: false),
                    ContractFeatures = table.Column<byte>(nullable: false),
                    RateBookId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Rates_VehicleVariants_CapCode",
                        column: x => x.CapCode,
                        principalTable: "VehicleVariants",
                        principalColumn: "CapCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Rates_RateBooks_RateBookId",
                        column: x => x.RateBookId,
                        principalTable: "RateBooks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Rates_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BackgroundQueueWorkItems_Queued",
                table: "BackgroundQueueWorkItems",
                column: "Queued");

            migrationBuilder.CreateIndex(
                name: "IX_BackgroundQueueWorkItems_Type",
                table: "BackgroundQueueWorkItems",
                column: "Type");

            migrationBuilder.CreateIndex(
                name: "IX_InvalidRateBookRows_RateBookId",
                table: "InvalidRateBookRows",
                column: "RateBookId");

            migrationBuilder.CreateIndex(
                name: "IX_RateBooks_Filename",
                table: "RateBooks",
                column: "Filename");

            migrationBuilder.CreateIndex(
                name: "IX_Rates_CapCode",
                table: "Rates",
                column: "CapCode");

            migrationBuilder.CreateIndex(
                name: "IX_Rates_ContractFeatures",
                table: "Rates",
                column: "ContractFeatures");

            migrationBuilder.CreateIndex(
                name: "IX_Rates_ContractSubject",
                table: "Rates",
                column: "ContractSubject");

            migrationBuilder.CreateIndex(
                name: "IX_Rates_Mileage",
                table: "Rates",
                column: "Mileage");

            migrationBuilder.CreateIndex(
                name: "IX_Rates_Provider",
                table: "Rates",
                column: "Provider");

            migrationBuilder.CreateIndex(
                name: "IX_Rates_RateBookId",
                table: "Rates",
                column: "RateBookId");

            migrationBuilder.CreateIndex(
                name: "IX_Rates_Term",
                table: "Rates",
                column: "Term");

            migrationBuilder.CreateIndex(
                name: "IX_Rates_VehicleId",
                table: "Rates",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_Manufacturer",
                table: "Vehicles",
                column: "Manufacturer");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_Model",
                table: "Vehicles",
                column: "Model");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleVariants_VehicleId",
                table: "VehicleVariants",
                column: "VehicleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BackgroundQueueWorkItems");

            migrationBuilder.DropTable(
                name: "InvalidRateBookRows");

            migrationBuilder.DropTable(
                name: "Rates");

            migrationBuilder.DropTable(
                name: "VehicleVariants");

            migrationBuilder.DropTable(
                name: "RateBooks");

            migrationBuilder.DropTable(
                name: "Vehicles");
        }
    }
}
