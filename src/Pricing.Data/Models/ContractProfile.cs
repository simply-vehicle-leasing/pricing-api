﻿namespace Pricing.Data.Models
{
    public readonly struct ContractProfile
    {
        public ContractProfile(short term, int mileage)
        {
            Term = term;
            Mileage = mileage;
        }

        public short Term { get; }

        public int Mileage { get; }

        public bool Equals(ContractProfile other) => Term == other.Term && Mileage == other.Mileage;

        public override bool Equals(object obj) => obj != null && obj is ContractProfile profile && Equals(profile);

        public override int GetHashCode()
        {
            unchecked
            {
                return (Term * 397) ^ Mileage;
            }
        }

        public override string ToString() => $"{Term} months, {Mileage} miles";

        public void Deconstruct(out int term, out int mileage)
        {
            term = Term;
            mileage = Mileage;
        }
    }
}
