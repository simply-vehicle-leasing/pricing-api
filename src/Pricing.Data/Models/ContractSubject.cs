﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pricing.Data.Models
{
    [Flags]
    public enum ContractSubject : byte
    {
        [Display(Name = "Personal")]
        Personal = 1,

        [Display(Name = "Business")]
        Business = 2
    }
}
