﻿using System.ComponentModel.DataAnnotations;

namespace Pricing.Data.Models
{
    public enum RateBookProvider : byte
    {
        [Display(Name = "LEX")]
        LexAutolease = 1,

        [Display(Name = "Hitachi")]
        Hitachi = 2,

        [Display(Name = "Ogilvie")]
        Ogilvie = 3
    }
}
