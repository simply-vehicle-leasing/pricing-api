﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pricing.Data.Models
{
    [Flags]
    public enum ContractFeatures : byte
    {
        [Display(Name = "None")]
        None = 0,

        [Display(Name = "Maintained")]
        Maintained = 0x01,

        [Display(Name = "Tyres")]
        Tyres = 0x02,

        [Display(Name = "Recovery")]
        Recovery = 0x04
    }
}
