param([Boolean]$Build=$true)

. .\Include.ps1

if ($Build) {
    Exec { dotnet restore } "Failed to restore NuGet packages"

    # Publish
    Publish "Pricing.Api"
    
    # Test
    # Test "Pricing.Api.Tests"
}

# Run everything in containers
Compose "full-stack"